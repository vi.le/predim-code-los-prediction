<!--
SPDX-License-Identifier: GPL-3.0-only
SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE U1290, DISP UR4570
-->

The objective of this work was to predict hospital length of stay for all
patients with administrative data from acute and emergency care. A deep learning
model leveraging the embeddings' mechanism was trained to predict length of stay
at each step of the patient's pathway, and was compared to a random forest and a
logistic regression.

# Requirements

The following python packages are needed.

| **Package**  | **Purpose**                                                                                 | **Version used** |
|--------------|---------------------------------------------------------------------------------------------|------------------|
| Ray.tune     | Hyperparameter tuning                                                                       | 1.2              |
| datetime     | Measuring time elapsed during training                                                      |                  |
| json         | Storing the performance metrics                                                             |                  |
| matplotlib   | Visualization                                                                               |                  |
| networkx     | Creating a tree of binary classifiers (experimental)                                        |                  |
| nevergrad    | Hyperparameter tuning                                                                       |                  |
| numpy        | Swiss-army knife                                                                            |                  |
| pandas       | Data preprocessing                                                                          | 1.1              |
| pickle       | Storing trained models                                                                      |                  |
| pytorch      | Training the neural network                                                                 | 1.9              |
| scikit learn | Preprocessing, cross-validation, random forest and logistic regression, performance metrics | 0.24             |

# Files description

| File                                 | Description                                                                          |
|--------------------------------------|--------------------------------------------------------------------------------------|
| `notebooks/analyze_main_res.ipynb` | Create training chart for training loss and test accuracy                            |
| `notebooks/table1.ipynb`           | Feature engineering                                                                  |
| `src/bst.py`                       | Experimental code for predicting LOS with a tree of models                           |
| `src/bst_main.py`                  | Training for experimental code for predicting LOS with a tree of models              |
| `src/dataset.py`                   | data preprocessing                                                                   |
| `src/evaluate.py`                  | Performance metrics                                                                  |
| `src/int-grads.py`                 | Try to provide prediction interpretations                                            |
| `src/main.py`                      | train the Neural network model                                                       |
| `src/main_ml.py`                   | train the random forest model                                                        |
| `src/main_reg.py`                  | train the logistic regression model                                                  |
| `src/model.py`                     | neural network model                                                                 |
| `src/test.py`                      | Run evaluation metrics and create confusion matrix                                   |
| `src/test_bland_altman.py`         | Create a Bland-Altman chart                                                          |
| `src/train.py`                     | Training of the neural network, machine learning model and logistic regression model |
| `src/tune_dl.py`                   | hyperparameter tuning for the neural network                                         |
| `src/tune_ml.py`                   | hyperparameter tuning for the random forest (was also used for logistic regression)  |

# How to use

For the preprocessing, run a [jupyter](https://jupyter.org/install) notebook and
point it to [`table1.ipynb`](notebooks/table1.ipynb). For model training, run a
file with "main" in its name. For performance evaluation, see `src/test*.py`
and [`analyze_main_res.py`](notebooks/analyze_main_res.ipynb).

# Attribution

This code has been developed during the PhD thesis of Vincent Lequertier (1, 2),
supervised by Antoine Duclos (1), Julien Fondrevelle (2) and Tao Wang (3).

1. Université Claude Bernard Lyon 1, Research on Healthcare Performance
(RESHAPE), INSERM U1290, Lyon, France
2. Univ. Lyon, INSA-Lyon, DISP, UR4570, 69621 Villeurbanne cedex, France
3. Univ. Lyon, UJM-Saint-Etienne, DISP, UR4570, 69621 Villeurbanne cedex, France

This work was partially supported by the European Research Ambition Pack 2018
grant, distributed by the French Auvergne Rhône-Alpes region.

# Licensing

The code is released under the terms of the GNU Public License 3.0. Please see
the [`LICENSE`](LICENSE) file.
