# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

from math import sqrt
from sklearn.preprocessing import (
    KBinsDiscretizer,
    LabelEncoder,
    MinMaxScaler,
    OrdinalEncoder,
    OneHotEncoder,
    StandardScaler,
)
import pandas as pd


def format_dataset_dl(data_path: str, ordinal_cols, cont_cols):
    # Take the preprocessed dataset and change its representation so that it's
    # compatible with DL algs. Encoders don't deal well with missing
    # values. Data needs to be numberical so format cateogires with ordinal
    # encoding to use embeddings and standardize / normalize continuous
    x = pd.read_csv(data_path, sep="\t", encoding="utf-8")
    # sample a part of the training data. complete dataset could not be tested
    # with hyperparameters tuning because of memory limitations
    x = x.sample(frac=0.99)
    ordinal_enc = OrdinalEncoder(dtype=int)
    x[ordinal_cols] = ordinal_enc.fit_transform(x[ordinal_cols])
    cont_enc = StandardScaler()
    x[cont_cols] = cont_enc.fit_transform(
        MinMaxScaler(feature_range=(-1, 1)).fit_transform(x[cont_cols])
    )
    x["los"] = format_los(x.los)
    label_enc = LabelEncoder()
    y = pd.Series(label_enc.fit_transform(x.pop("los")))

    return (x, y, ordinal_enc, label_enc, cont_enc)


def format_dataset_dl_binary(data_path: str, ordinal_cols, cont_cols, day: int = 3):
    # Take the preprocessed dataset and change its representation so that it's
    # compatible with DL algs. Encoders don't deal well with missing
    # values. Data needs to be numberical so format cateogires with ordinal
    # encoding to use embeddings and standardize / normalize continuous
    x = pd.read_csv(data_path, sep="\t", encoding="utf-8")
    # sameple a part of the training data. complete dataset could not be tested
    # with hyperparameters tuning because of memory limitations
    x = x.sample(frac=0.99)
    ordinal_enc = OrdinalEncoder(dtype=int)
    x[ordinal_cols] = ordinal_enc.fit_transform(x[ordinal_cols])
    cont_enc = StandardScaler()
    x[cont_cols] = cont_enc.fit_transform(
        MinMaxScaler(feature_range=(-1, 1)).fit_transform(x[cont_cols])
    )
    x["los"] = format_los_binary(x.los, day)
    label_enc = LabelEncoder()
    y = pd.Series(label_enc.fit_transform(x.pop("los")))

    return (x, y, ordinal_enc, label_enc, cont_enc)


def format_dataset_ml(data_path: str, ordinal_cols, cont_cols):
    # Transform everything to categorical
    x = pd.read_csv(data_path, sep="\t", encoding="utf-8")

    ordinal_enc = OneHotEncoder(dtype=int, sparse=False)
    ordinal_enc.fit(x[ordinal_cols])
    x = pd.concat(
        [
            x[cont_cols + ["los"]].reset_index(drop=True),
            pd.DataFrame(
                ordinal_enc.transform(x[ordinal_cols]),
                columns=ordinal_enc.get_feature_names(ordinal_cols),
            ),
        ],
        axis=1,
    )
    # cont_enc = KBinsDiscretizer(encode="ordinal")
    # n_bins = [int(sqrt(len(x[col].astype(int).unique()))) for col in cont_cols]
    # x[cont_cols] = cont_enc.fit_transform(x[cont_cols])
    x["los"] = format_los(x.los)
    label_enc = LabelEncoder()
    y = pd.Series(label_enc.fit_transform(x.pop("los")))

    # return x, y, ordinal_enc, label_enc, cont_enc
    return x, y, ordinal_enc, label_enc


def format_dataset_reg(data_path: str, ordinal_cols, cont_cols):
    # Transform everything to categorical
    x = pd.read_csv(data_path, sep="\t", encoding="utf-8")
    # x = x.sample(frac=0.001)
    ordinal_enc = OneHotEncoder(dtype=int, sparse=False)
    ordinal_enc.fit(x[ordinal_cols])
    print(x.shape)
    x = pd.concat(
        [
            x[cont_cols + ["los"]].reset_index(drop=True),
            pd.DataFrame(
                ordinal_enc.transform(x[ordinal_cols]),
                columns=ordinal_enc.get_feature_names(ordinal_cols),
            ),
        ],
        axis=1,
    )
    print(x)
    print(x.shape)
    # cont_enc = KBinsDiscretizer(encode="ordinal")
    # n_bins = [int(sqrt(len(x[col].astype(int).unique()))) for col in cont_cols]
    # x[cont_cols] = cont_enc.fit_transform(x[cont_cols])
    x["los"] = format_los(x.los)
    label_enc = LabelEncoder()
    x["los"] = label_enc.fit_transform(x["los"])

    return (
        x[list(filter(lambda col: col != "los", x.columns))],
        x["los"],
        ordinal_enc,
        label_enc,
    )


def format_dataset_reg_binary(data_path: str, ordinal_cols, cont_cols, day: int = 3):
    # Transform everything to categorical
    x = pd.read_csv(data_path, sep="\t", encoding="utf-8")
    # x = x.sample(frac=0.001)
    ordinal_enc = OneHotEncoder(dtype=int, sparse=False)
    ordinal_enc.fit(x[ordinal_cols])
    x = pd.concat(
        [
            x[cont_cols + ["los"]].reset_index(drop=True),
            pd.DataFrame(
                ordinal_enc.transform(x[ordinal_cols]),
                columns=ordinal_enc.get_feature_names(ordinal_cols),
            ),
        ],
        axis=1,
    )
    # cont_enc = KBinsDiscretizer(encode="ordinal")
    # n_bins = [int(sqrt(len(x[col].astype(int).unique()))) for col in cont_cols]
    # x[cont_cols] = cont_enc.fit_transform(x[cont_cols])
    x["los"] = format_los_binary(x.los, day)
    label_enc = LabelEncoder()
    x["los"] = label_enc.fit_transform(x["los"])

    return (
        x[list(filter(lambda col: col != "los", x.columns))],
        x["los"],
        ordinal_enc,
        label_enc,
    )


def format_los(los: pd.Series):
    return los.apply(
        lambda los: 0
        if los == 0
        else "1"
        if los == 1
        else "2"
        if los == 2
        else "3"
        if los == 3
        else "4"
        if los == 4
        else "5"
        if los == 5
        else "6"
        if los == 6
        else "7"
        if los == 7
        else "8"
        if los == 8
        else "9"
        if los == 9
        else "10"
        if los == 10
        else "11"
        if los == 11
        else "12"
        if los == 12
        else "13"
        if los == 13
        else "14+"
    ).astype(str)


def format_los_binary(los: pd.Series, n: int = 3):
    return los.apply(
        lambda los: str(n) + " days" if los == n else "not " + str(n) + " days"
    )
