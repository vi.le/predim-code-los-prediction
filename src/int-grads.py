# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import pandas as pd
import pickle
from model import los
import torch
import json
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, AutoMinorLocator
import numpy as np
from captum.attr import IntegratedGradients
from captum.attr._models.base import configure_interpretable_embedding_layer

plt.rcParams.update(
    {
        "text.usetex": True,
        "font.family": "sans-serif",
        "text.latex.preamble": r"\usepackage[scaled]{helvet}\renewcommand\familydefault{\sfdefault}\usepackage[helvet]{sfmath}\everymath={\sf}",
    }
)

x_df = pd.read_csv("x_df_to_eval.csv")
y_df = pd.read_csv("y_df_to_eval.csv")
with open("ord_enc_to_eval", "rb") as fh:
    ord_enc = pickle.load(fh)
with open("label_enc_to_eval", "rb") as fh:
    label_enc = pickle.load(fh)
with open("cont_enc_to_eval", "rb") as fh:
    cont_enc = pickle.load(fh)


with open("../notebooks/rename.json", "r") as fh:
    rename = json.load(fh)

groups = {}
with open("../desc_variable_groups.csv", "r") as fh:
    # Skip header
    next(fh)
    for line in fh.readlines():
        variable_group = line.split("\t")
        groups[variable_group[0]] = variable_group[2].rstrip()
with open("../notebooks/elixhauser", "r") as fh:
    elix_list = [line.split(",")[0] for line in fh.readlines()]

rename["dentj"] = "Entry day"

torch.set_num_threads(5)

x_df.drop(
    [
        "Unnamed: 0",
    ],
    axis=1,
    inplace=True,
)

y_df.drop(
    [
        "Unnamed: 0",
    ],
    axis=1,
    inplace=True,
)

cont_cols = (
    [
        "age",
        "mean_los_um_n_days",
        "mean_los_diag_n_days",
        "mean_los_n_days",
        "n_adm_n_days",
        "n_adm_n_days_diag",
        "n_adm_n_days_um",
        "n_adm_n_days_patient",
        "time_already_spent_stay_patient",
        # "time_already_spent_n_days_patient",
        "Number of elixhauser commorbidities",
        "Number of commorbidities",
        "n_acts",
        "urg_n_commorbidities",
        "urg_n_acts",
        "urg_adm_n_days",
        "urg_n_adm_n_days_diag",
        "urg_n_adm_n_days_patient",
    ]
    + ["mean_los_diag_cat_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
    # + ["mean_los_diag_cat_um_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
)


ordinal_cols = [
    "diag_category",
    "urg_diag_category",
    "dentmo",
    "denta",
    # "dentdow",
    "dentj",
    "denttod",
    "hopital",
    "typeauto",
    "umano",
    "sexe",
    "modent",
    "Congestive heart failure",
    "Cardiac arrhythmias",
    "Valvular disease",
    "Pulmonary circulation disorders",
    "Peripheral vascular disorders",
    "Hypertension uncomplicated",
    "Hypertension complicated",
    "Paralysis",
    "Other neurological disorders",
    "Chronic pulmonary disease",
    "Diabetes uncomplicated",
    "Diabetes complicated",
    "Hypothyroidism",
    "Renal failure",
    "Liver disease",
    "Metastatic cancer",
    "Solid tumour without metastasis",
    "Rheumatoid arthritis/collagen vascular diseases",
    "Coagulopathy",
    "Obesity",
    "Weight loss",
    "Fluid and electrolyte disorders",
    "Blood loss anaemia",
    "Deficiency anaemia",
    "Alcohol abuse",
    "Depression",
    "AAFA001",
    "AAFA002",
    "AAFA007",
    "AALA004",
    "AALB001",
    "AALB002",
    "ABCB001",
    "ABLB003",
    "ACFA002",
    "ACFA007",
    "ACFA013",
    "ACFA028",
    "ACPA001",
    "AFLB008",
    "AGLB001",
    "DBKA006",
    "DBMA002",
    "DDAF008",
    "DDMA031",
    "DDQH009",
    "DENF018",
    "DEPF033",
    "DEQF004",
    "DZEA002",
    "EASF010",
    "EBFA008",
    "EBLA003",
    "EPLF005",
    "EQLF004",
    "EQQF004",
    "GEJE001",
    "GEQE004",
    "GEQE007",
    "GEQE009",
    "GEQE012",
    "HEQE002",
    "HHFA016",
    "HJQE001",
    "HPFA003",
    "HPFA004",
    "JAEA003",
    "JQGA002",
    "JQGA003",
    "JQGA004",
    "JVJF002",
    "JVJF004",
    "JVJF005",
    "JVJF008",
    "KCFA005",
    "NBCA006",
    "NBCA010",
    "NEKA020",
    "ZCQA001",
    "dureeurg",
    "ccmu",
    "igs2",
    "cmuc",
    "went_to_emergency_care",
    "RUMNU",
]

model = los(
    layers=[512, 256, 192, 128, 64, 32],
    emb_sz_factor=7,
    use_bn=False,
    ps=0,
    emb_drop=0,
    # emb_szs=len(ordinal_cols),
    emb_szs=[len(cats) for cats in ord_enc.categories_],
    n_cont=len(cont_cols),
    out_sz=len(label_enc.classes_),
)


checkpoint = torch.load("model_fold_1")

model.load_state_dict(checkpoint["model"])

x_tst_fold = x_df.iloc[checkpoint["test_idx"]][:10000]
y_tst_fold = y_df.iloc[checkpoint["test_idx"]][:10000]
test_input_tensor_ord = torch.from_numpy(x_tst_fold[ordinal_cols].values).type(
    torch.FloatTensor
)
test_input_tensor_cont = torch.from_numpy(x_tst_fold[cont_cols].values).type(
    torch.FloatTensor
)

# Compute embedding output for given inputs
embedding_inp = [
    e(test_input_tensor_ord.long()[:, i]) for i, e in enumerate(model.embeds)
]
embeddings_cat = torch.cat(embedding_inp, 1)


tst = torch.utils.data.TensorDataset(
    torch.Tensor(x_tst_fold.values),
    torch.LongTensor(y_tst_fold.values.squeeze()),
)
tst_loader = torch.utils.data.DataLoader(tst, batch_size=1024, shuffle=False)

y_preds = []
with torch.inference_mode():
    for (x, y) in tst_loader:
        x_cat = x[:, : len(ordinal_cols)].long()
        x_cont = x[:, len(ordinal_cols) :]
        preds = model(x_cat, x_cont).argmax(dim=1)
        y_preds.append(preds)
preds = torch.LongTensor([y for x in y_preds for y in x])

# Replace embedding layers with IntepretableEmbeddings,
# which just directly return the input (assuming embeddings are given as input)
interp_embeddings = []
for i in range(len(model.embeds)):
    interp_embeddings.append(
        configure_interpretable_embedding_layer(model, "embeds." + str(i))
    )

ig = IntegratedGradients(model)

test_input_tensor_ord.requires_grad_()
test_input_tensor_cont.requires_grad_()

attr, delta = ig.attribute(
    inputs=(embeddings_cat, test_input_tensor_cont),
    # target=torch.LongTensor(y_tst_fold.values).squeeze(),
    target=preds,
    return_convergence_delta=True,
    # Specify to forward that we are in captum, dont compute embs
    additional_forward_args=True,
)


attr = torch.cat((attr[0], attr[1]), axis=1).detach().numpy()

# Helper method to print importances and visualize distribution
def visualize_importances(
    feature_names_cat: list,
    feature_names_cont: list,
    importances: list,
    title: str = "Average Feature Importances",
    plot: bool = True,
):
    print(title)
    importances_cat = []
    for i in range(len(feature_names_cat)):
        # TODO check for off by one
        size_cat = model.embeds[i].embedding_dim
        start = sum([model.embeds[n].embedding_dim for n in range(0, i)])
        importances_cat.append(importances[start : start + size_cat].sum())
    total_emb = sum(
        [model.embeds[n].embedding_dim for n in range(len(feature_names_cat))]
    )
    importances_cont = []
    for i in range(len(feature_names_cont)):
        importances_cont.append(importances[total_emb + i])
    features = feature_names_cat + feature_names_cont
    importances = importances_cat + importances_cont

    features_groups = list(set(groups.values()))
    importances_group = {}
    for idx, importance in enumerate(importances):
        group = groups[features[idx]]
        if not group in importances_group:
            importances_group[group] = 0
        importances_group[group] += importance

    importances_group = [importances_group[group] for group in features_groups]
    idx = np.argsort([n for n in importances])[::-1]
    features = np.array(features)[idx][:50]
    importances = np.array(importances)[idx][:50]
    idx = np.argsort([n for n in importances_group])[::-1]
    features_groups = np.array(features_groups)[idx]
    importances_group = np.array(importances_group)[idx]
    for idx, col in enumerate(features):
        if col in rename:
            features[idx] = rename[col]
    if plot:
        fig, axes = plt.subplots(1, 2, figsize=(20, 15))
        x_pos_0 = np.arange(len(features))
        axes[0].bar(x_pos_0, importances, align="center")
        axes[0].set_yscale("log")
        axes[0].set_xticks(x_pos_0)
        axes[0].set_xticklabels(features, rotation=90)
        axes[0].set_xlabel("variable")
        axes[0].set_ylabel("attribution (logarithmic scale)")
        axes[0].grid(which="major", axis="y", linestyle=":")
        axes[0].grid(False, which="major", axis="x")
        axes[0].set_title(
            "Attribution of each variable with regard to the predicted class (top 50)",
            fontsize=14,
        )
        x_pos_1 = np.arange(len(features_groups))
        print(features_groups)
        print(importances_group)
        axes[1].bar(
            x_pos_1,
            importances_group,
            align="center",
        )
        axes[1].set_xticks(x_pos_1)
        axes[1].set_xticklabels(features_groups)
        axes[1].set_xlabel("variable group")
        axes[1].set_ylabel("attribution")
        axes[1].yaxis.set_major_locator(MultipleLocator(1))
        axes[1].yaxis.set_minor_locator(AutoMinorLocator())
        axes[1].grid(which="major", axis="y", linestyle=":")
        axes[1].grid(False, which="major", axis="x")
        axes[1].set_title(
            "Attribution of each variable group with regard to the predicted class",
            fontsize=15,
        )
        fig.suptitle("Mean attribution of each variable on the test set", fontsize=16)
        plt.tight_layout()
        fig.savefig("attr.png", dpi=300)
        fig.savefig("attr.eps", dpi=300)
        fig.savefig("attr.pdf", dpi=300)


visualize_importances(ordinal_cols, cont_cols, np.mean(attr, axis=0), plot=True)
