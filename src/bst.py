# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import networkx as nx


class LOSBST:
    """Create a hierarchical tree of binary classifiers"""

    def __init__(self, days: list, n_days: int = 7, balanced=False):
        self.G = nx.DiGraph()
        self.n_days = n_days
        self.days = days
        self.balanced = balanced

        # If balanced, don't take into account the distributions of lenfth of
        # stays
        if balanced:
            days = list(range(1, n_days + 1))
        self._gen_tree(days, "root")

    def _gen_tree(self, days: list, root: str) -> None:
        """Generate the binary tree"""
        if len(set(days)) <= 2:
            # We're right before the leaves
            # Create them and return
            if self.balanced:
                for day in sorted(list(set(days))):
                    self.G.add_edge(root, "j" + str(day))
            else:
                # Output only the leaf on the left. Don't add the last leaf
                if sorted(list(set(days)))[0] != self.n_days - 1:
                    self.G.add_edge(root, "j" + str(sorted(list(set(days)))[0]))
            return

        # Divide the days in two part based on the median. This is good to fight
        # class imbalance
        median_idx, median = self.median(days)
        # Add left edge
        self.G.add_edge(root, str(-median))
        # Add right edge
        self.G.add_edge(root, str(median))

        # Go down in the tree, the roots are each sides of the current median
        right, left = days[:median_idx], days[median_idx:]
        self._gen_tree(right, str(-median))
        self._gen_tree(left, str(median))

    def get_path(self) -> list:
        models = []
        for node in self.G.nodes():
            successors = list(self.G.successors(node))
            if len(successors) > 1:
                # Get the parents of the node. Used to filter out part of the
                # tree node not part of the dirsect path from root to node
                parents = list(
                    map(
                        int,
                        filter(
                            lambda node: node != "root",
                            nx.shortest_path(self.G, "root", node),
                        ),
                    )
                )
                models.append({"name": successors, "parents": parents})

        return models

    def draw(self) -> None:
        nx.drawing.nx_pydot.write_dot(self.G, "test.dot")
        graph = nx.drawing.nx_pydot.to_pydot(self.G).write_png("bst_graph.png")

    def median(self, x: list) -> int:
        """Self-explanatory"""
        if self.balanced:
            x = sorted(set(x))
            length = len(x)
            median_indice = (length) // 2

            return median_indice, x[median_indice]
        else:
            x = sorted(x)
            length = len(x)
            # First quartile
            median_indice = (length - 1) // 6
            if length % 2:
                return median_indice, x[median_indice]
            else:
                return median_indice, x[median_indice + 1]
