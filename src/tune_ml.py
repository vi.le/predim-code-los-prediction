# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import dataset
from model import los
from evaluate import evaluate
import os
from tune_sklearn import TuneGridSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn.utils.class_weight import compute_class_weight
from sklearn.ensemble import RandomForestClassifier
import nevergrad as ng
import numpy as np


ordinal_cols = [
    "diag_category",
    "urg_diag_category",
    "dentmo",
    "denta",
    # "dentdow",
    "dentj",
    "denttod",
    "hopital",
    "typeauto",
    "umano",
    "sexe",
    "modent",
    "Congestive heart failure",
    "Cardiac arrhythmias",
    "Valvular disease",
    "Pulmonary circulation disorders",
    "Peripheral vascular disorders",
    "Hypertension uncomplicated",
    "Hypertension complicated",
    "Paralysis",
    "Other neurological disorders",
    "Chronic pulmonary disease",
    "Diabetes uncomplicated",
    "Diabetes complicated",
    "Hypothyroidism",
    "Renal failure",
    "Liver disease",
    "Metastatic cancer",
    "Solid tumour without metastasis",
    "Rheumatoid arthritis/collagen vascular diseases",
    "Coagulopathy",
    "Obesity",
    "Weight loss",
    "Fluid and electrolyte disorders",
    "Blood loss anaemia",
    "Deficiency anaemia",
    "Alcohol abuse",
    "Depression",
    "AAFA001",
    "AAFA002",
    "AAFA007",
    "AALA004",
    "AALB001",
    "AALB002",
    "ABCB001",
    "ABLB003",
    "ACFA002",
    "ACFA007",
    "ACFA013",
    "ACFA028",
    "ACPA001",
    "AFLB008",
    "AGLB001",
    "DBKA006",
    "DBMA002",
    "DDAF008",
    "DDMA031",
    "DDQH009",
    "DENF018",
    "DEPF033",
    "DEQF004",
    "DZEA002",
    "EASF010",
    "EBFA008",
    "EBLA003",
    "EPLF005",
    "EQLF004",
    "EQQF004",
    "GEJE001",
    "GEQE004",
    "GEQE007",
    "GEQE009",
    "GEQE012",
    "HEQE002",
    "HHFA016",
    "HJQE001",
    "HPFA003",
    "HPFA004",
    "JAEA003",
    "JQGA002",
    "JQGA003",
    "JQGA004",
    "JVJF002",
    "JVJF004",
    "JVJF005",
    "JVJF008",
    "KCFA005",
    "NBCA006",
    "NBCA010",
    "NEKA020",
    "ZCQA001",
    "dureeurg",
    "ccmu",
    "igs2",
    "cmuc",
    "went_to_emergency_care",
    "RUMNU",
]

one_hot_cols = []
emb_cols = ordinal_cols + one_hot_cols
cont_cols = [
    "age",
    "mean_los_um_n_days",
    "mean_los_diag_n_days",
    "mean_los_n_days",
    "n_adm_n_days",
    "n_adm_n_days_diag",
    "n_adm_n_days_um",
    "n_adm_n_days_patient",
    "time_already_spent_stay_patient",
    # "time_already_spent_n_days_patient",
    "Number of elixhauser commorbidities",
    "Number of commorbidities",
    "n_acts",
    "urg_n_commorbidities",
    "urg_n_acts",
    "urg_adm_n_days",
    "urg_n_adm_n_days_diag",
    "urg_n_adm_n_days_patient",
] + ["mean_los_diag_cat_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]


x_df, y_df, ordinal_enc, label_enc = dataset.format_dataset_ml(
    "../notebooks/pmsi_full.csv", ordinal_cols, cont_cols
)

print("tuning on {} RUM".format(len(x_df)))

# Remove los_cont since it's no longer needed, also remove identifiers because
# they do not matter for predictions
# x_df.drop(
#     [
#         "Unnamed: 0",
#         "los_cont",
#         "rssnuano",
#         "ippano",
#         "level_0",
#     ],
#     axis=1,
#     inplace=True,
# )
# x_df = x_df.reindex(columns=ordinal_cols + cont_cols, copy=False)

data_dir = os.path.abspath("./data")

config = {
    "n_estimators": list(range(100, 550, 50)),
    "criterion": ["gini", "entropy"],
    "max_depth": [10, 20, 30, 40, 50],
    "max_features": ["sqrt", "log2"],
    "min_samples_split": [0.1, 0.01, 0.001],
    "bootstrap": [True, False],
    "class_weight": ["balanced"],
}

tune_search = GridSearchCV(RandomForestClassifier(), config, verbose=2, n_jobs=5)
tune_search.fit(x_df.values, y_df.values)
print(tune_search.best_params_)
print(tune_search.best_score_)
