# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import dataset
from model import los
from evaluate import evaluate
import os
import ray
import torch
from datetime import datetime
import time
from hyperopt import hp
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler
from ray.tune.suggest.nevergrad import NevergradSearch
from ray.tune.suggest.optuna import OptunaSearch
from ray.tune.suggest.hyperopt import HyperOptSearch
from ray.tune.suggest import ConcurrencyLimiter
from sklearn.model_selection import train_test_split
from sklearn.utils.class_weight import compute_class_weight
import nevergrad as ng
import numpy as np


ray.init(
    num_cpus=16,
)


ordinal_cols = [
    "diag_category",
    "urg_diag_category",
    "dentmo",
    "denta",
    # "dentdow",
    "dentj",
    "denttod",
    "hopital",
    "typeauto",
    "umano",
    "sexe",
    "modent",
    "Congestive heart failure",
    "Cardiac arrhythmias",
    "Valvular disease",
    "Pulmonary circulation disorders",
    "Peripheral vascular disorders",
    "Hypertension uncomplicated",
    "Hypertension complicated",
    "Paralysis",
    "Other neurological disorders",
    "Chronic pulmonary disease",
    "Diabetes uncomplicated",
    "Diabetes complicated",
    "Hypothyroidism",
    "Renal failure",
    "Liver disease",
    "Metastatic cancer",
    "Solid tumour without metastasis",
    "Rheumatoid arthritis/collagen vascular diseases",
    "Coagulopathy",
    "Obesity",
    "Weight loss",
    "Fluid and electrolyte disorders",
    "Blood loss anaemia",
    "Deficiency anaemia",
    "Alcohol abuse",
    "Depression",
    "AAFA001",
    "AAFA002",
    "AAFA007",
    "AALA004",
    "AALB001",
    "AALB002",
    "ABCB001",
    "ABLB003",
    "ACFA002",
    "ACFA007",
    "ACFA013",
    "ACFA028",
    "ACPA001",
    "AFLB008",
    "AGLB001",
    "DBKA006",
    "DBMA002",
    "DDAF008",
    "DDMA031",
    "DDQH009",
    "DENF018",
    "DEPF033",
    "DEQF004",
    "DZEA002",
    "EASF010",
    "EBFA008",
    "EBLA003",
    "EPLF005",
    "EQLF004",
    "EQQF004",
    "GEJE001",
    "GEQE004",
    "GEQE007",
    "GEQE009",
    "GEQE012",
    "HEQE002",
    "HHFA016",
    "HJQE001",
    "HPFA003",
    "HPFA004",
    "JAEA003",
    "JQGA002",
    "JQGA003",
    "JQGA004",
    "JVJF002",
    "JVJF004",
    "JVJF005",
    "JVJF008",
    "KCFA005",
    "NBCA006",
    "NBCA010",
    "NEKA020",
    "ZCQA001",
    "dureeurg",
    "ccmu",
    "igs2",
    "cmuc",
    "went_to_emergency_care",
    "RUMNU",
]

one_hot_cols = []
emb_cols = ordinal_cols + one_hot_cols
cont_cols = (
    [
        "age",
        "mean_los_um_n_days",
        "mean_los_diag_n_days",
        "mean_los_n_days",
        "n_adm_n_days",
        "n_adm_n_days_diag",
        "n_adm_n_days_um",
        "n_adm_n_days_patient",
        "time_already_spent_stay_patient",
        # "time_already_spent_n_days_patient",
        "Number of elixhauser commorbidities",
        "Number of commorbidities",
        "n_acts",
        "urg_n_commorbidities",
        "urg_n_acts",
        "urg_adm_n_days",
        "urg_n_adm_n_days_diag",
        "urg_n_adm_n_days_patient",
    ]
    + ["mean_los_diag_cat_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
    # + ["mean_los_diag_cat_um_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
)

x_df, y_df, ordinal_enc, label_enc, cont_enc = dataset.format_dataset_dl(
    "../notebooks/pmsi_full.csv", ordinal_cols, cont_cols
)

print("tuning on {} RUM".format(len(x_df)))
print("tuning on {} patients".format(len(x_df["ippano"].unique())))
print("tuning on {} stays".format(len(x_df["rssnuano"].unique())))

# Remove los_cont since it's no longer needed, also remove identifiers because
# they do not matter for predictions
x_df.drop(
    [
        "Unnamed: 0",
        "los_cont",
        "rssnuano",
        "ippano",
    ],
    axis=1,
    inplace=True,
)

# this makes it easier to select categorical and numerical columns
x_df = x_df.reindex(columns=ordinal_cols + cont_cols, copy=False)


def train_tune(config, checkpoint_dir=None, data=None):
    x_df, y_df = data
    del data
    x_trn_fold, x_tst_fold, y_trn_fold, y_tst_fold = train_test_split(
        x_df, y_df, test_size=0.2
    )

    trn = torch.utils.data.TensorDataset(
        torch.Tensor(x_trn_fold.values), torch.LongTensor(y_trn_fold.values)
    )
    tst = torch.utils.data.TensorDataset(
        torch.Tensor(x_tst_fold.values), torch.LongTensor(y_tst_fold.values)
    )
    trn_loader = torch.utils.data.DataLoader(
        trn, batch_size=config["batch_size"], shuffle=True, num_workers=0
    )
    tst_loader = torch.utils.data.DataLoader(tst, batch_size=1024, shuffle=False)

    iters = len(trn_loader)

    model = los(
        emb_szs=[len(cats) for cats in ordinal_enc.categories_],
        n_cont=len(cont_cols),
        out_sz=len(label_enc.classes_),
        use_bn=config["use_bn"],
        layers=config["layers"],
        ps=config["ps"],
        emb_sz_factor=config["emb_sz_factor"],
    )
    weights = compute_class_weight(
        "balanced", classes=np.unique(y_trn_fold.values), y=y_trn_fold.values
    )
    # criterion = torch.nn.NLLLoss()
    criterion = torch.nn.NLLLoss(weight=torch.Tensor(weights))
    if config["optimizer"] == "momentum":
        optimizer = torch.optim.SGD(model.parameters(), lr=config["lr"], momentum=0.9)
    elif config["optimizer"] == "adam":
        optimizer = torch.optim.Adam(
            model.parameters(), lr=config["lr"], weight_decay=config["weight_decay"]
        )
    elif config["optimizer"] == "adamw":
        optimizer = torch.optim.AdamW(
            model.parameters(), lr=config["lr"], weight_decay=config["weight_decay"]
        )
    if checkpoint_dir:
        model_state, optimizer_state = torch.load(
            os.path.join(checkpoint_dir, "checkpoint")
        )
        model.load_state_dict(model_state)
        optimizer.load_state_dict(optimizer_state)

    for epoch in range(0, 20):
        for idx, (x, y) in enumerate(trn_loader, start=1):
            # For the embedings to work it should be long values
            x_cat = x[:, : len(ordinal_cols)].long()
            x_cont = x[:, len(ordinal_cols) :]
            optimizer.zero_grad()
            output = model(x_cat, x_cont)
            loss = criterion(output, y)
            loss.backward()
            optimizer.step()
            # scheduler.step(epoch + idx / iters)

        with tune.checkpoint_dir(epoch) as checkpoint_dir:
            path = os.path.join(checkpoint_dir, "checkpoint")
            torch.save((model.state_dict(), optimizer.state_dict()), path)

        model.eval()
        with torch.no_grad():
            res = evaluate(model, tst_loader, ordinal_cols)
        model.train()
        tune.report(acc=res[0], loss=res[1], kappa=res[2])


num_samples = 300
max_num_epochs = 50

data_dir = os.path.abspath("./data")
config = {
    "lr": tune.loguniform(1e-6, 1),
    "layers": tune.choice(
        [
            [1024, 512, 256, 128, 64, 32],
            [512, 256, 192, 128, 64, 32],
            [512, 256, 128, 64, 32],
            [192, 128, 64, 32],
            [256, 128, 64, 32],
            [128, 64, 32],
        ]
    ),
    "batch_size": tune.choice([128, 256, 512, 1024, 2048]),
    "optimizer": tune.choice(["momentum", "adam", "adamw"]),
    "ps": tune.choice([0, 0.01, 0.05]),
    "use_bn": tune.choice([True, False]),
    "emb_sz_factor": tune.quniform(1, 10, 0.5),
    "weight_decay": tune.loguniform(1e-6, 1e-1),
}
# Buggy nested lists whith hyperopt search space conversion. Thus it needs native HP
# space
# config = {
#     "lr": hp.loguniform("lr", 1e-6, 1),
#     "layers": hp.choice(
#         "layers",
#         [
#             [1024, 512, 256, 128, 64, 32],
#             [512, 256, 192, 128, 64, 32],
#             [512, 256, 128, 64, 32],
#             [192, 128, 64, 32],
#             [256, 128, 64, 32],
#             [128, 64, 32],
#         ],
#     ),
#     "batch_size": hp.choice("batch_size", [128, 256, 512, 1024, 2048]),
#     "optimizer": hp.choice("optimizer", ["momentum", "adam", "adamw"]),
#     "ps": hp.choice("ps", [0, 0.01, 0.05]),
#     "use_bn": hp.choice("use_bn", [True, False]),
#     "emb_sz_factor": hp.quniform("emb_sz_factor", 1, 10, 0.5),
#     "weight_decay": hp.loguniform("weight_decay", 1e-6, 1e-1),
# }

initial_state = [
    {
        "lr": 0.0059,
        "layers": [512, 256, 192, 128, 64, 32],
        "batch_size": 256,
        "optimizer": "adamw",
        "ps": 0.05,
        "use_bn": False,
        "emb_sz_factor": 3,
        "weight_decay": 0.000124261,
    }
]

scheduler = ASHAScheduler(
    time_attr="training_iteration",
    max_t=max_num_epochs,
    grace_period=5,
    reduction_factor=4,
)
alg = NevergradSearch(
    optimizer=ng.optimizers.TwoPointsDENoisy,
    metric="loss",
    mode="min",
)
# alg = OptunaSearch(metric="loss", mode="min")
# alg = HyperOptSearch(metric="loss", mode="min", space=config)
# alg = ConcurrencyLimiter(alg, max_concurrent=4)
reporter = CLIReporter(
    # parameter_columns=["l1", "l2", "lr", "batch_size"],
    metric_columns=["acc", "loss", "kappa", "training_iteration"]
)
result = tune.run(
    tune.with_parameters(train_tune, data=(x_df, y_df)),
    name="_".join(
        [alg.__class__.__name__, str(int(time.mktime(datetime.today().timetuple())))]
    ),
    resources_per_trial={"cpu": 4},
    # config=config,
    metric="loss",
    mode="min",
    num_samples=num_samples,
    scheduler=scheduler,
    search_alg=alg,
    progress_reporter=reporter,
)

best_trial = result.get_best_trial("loss", "min", "last")
print("Best trial config: {}".format(best_trial.config))
print("Best trial final validation loss: {}".format(best_trial.last_result["loss"]))
print("Best trial final validation accuracy: {}".format(best_trial.last_result["acc"]))
print("Best trial final validation kappa: {}".format(best_trial.last_result["kappa"]))
