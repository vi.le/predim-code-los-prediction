# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import pandas as pd
import pickle
from model import los
import torch
import dataset
from sklearn.model_selection import KFold
from evaluate import get_preds
import scipy.stats as st
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
from train import train_ml_bland_altman, train_reg_log_bland_altman
import numpy as np
import pandas as pd
from os.path import exists
import datashader as ds
from datashader.mpl_ext import dsshow
import string

from functools import partial


dyn = partial(ds.tf.dynspread, max_px=40, threshold=0.5)


def bland_altman_plot(data1, data2, ax, fig, *args, **kwargs):
    mean = np.mean([data1, data2], axis=0)
    diff = data1 - data2  # Difference between data1 and data2
    md = np.mean(diff)  # Mean of the difference
    sd = np.std(diff, axis=0)  # Standard deviation of the difference

    df = pd.DataFrame(dict(x=mean, y=diff))
    # see https://github.com/holoviz/datashader/issues/1082 for eps export
    # scaling issue
    da = dsshow(
        df,
        ds.Point("x", "y"),
        norm="log",
        aspect="auto",
        ax=ax,
        shade_hook=dyn,
        cmap="Blues",
    )
    plt.colorbar(da, ax=ax)

    ax.set_xticks(np.arange(0, 15))
    ax.set_yticks(np.arange(min(diff) - 2, max(diff) + 2, 1))
    ax.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax.yaxis.set_minor_locator(AutoMinorLocator(2))
    ax.set_xlabel("mean")
    ax.set_ylabel("difference")
    ax.axhline(md, color="gray", linestyle="--", linewidth=1.5)
    ax.axhline(md + 1.96 * sd, color="gray", linestyle="--", linewidth=1.5)
    ax.axhline(md - 1.96 * sd, color="gray", linestyle="--", linewidth=1.5)


plt.rcParams.update(
    {
        "text.usetex": True,
        "font.family": "sans-serif",
        "text.latex.preamble": r"\usepackage[scaled]{helvet}\renewcommand\familydefault{\sfdefault}\usepackage[helvet]{sfmath}\everymath={\sf}",
    }
)

x_df = pd.read_csv("x_df_to_eval.csv")
y_df = pd.read_csv("y_df_to_eval.csv")
with open("ord_enc_to_eval", "rb") as fh:
    ord_enc = pickle.load(fh)
with open("label_enc_to_eval", "rb") as fh:
    label_enc = pickle.load(fh)
with open("cont_enc_to_eval", "rb") as fh:
    cont_enc = pickle.load(fh)


x_df.drop(
    [
        "Unnamed: 0",
    ],
    axis=1,
    inplace=True,
)

y_df.drop(
    [
        "Unnamed: 0",
    ],
    axis=1,
    inplace=True,
)

cont_cols = [
    "age",
    "mean_los_um_n_days",
    "mean_los_diag_n_days",
    "mean_los_n_days",
    "n_adm_n_days",
    "n_adm_n_days_diag",
    "n_adm_n_days_um",
    "n_adm_n_days_patient",
    "time_already_spent_stay_patient",
    # "time_already_spent_n_days_patient",
    "Number of elixhauser commorbidities",
    "Number of commorbidities",
    "n_acts",
    "urg_n_commorbidities",
    "urg_n_acts",
    "urg_adm_n_days",
    "urg_n_adm_n_days_diag",
    "urg_n_adm_n_days_patient",
] + ["mean_los_diag_cat_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]

ordinal_cols = [
    "diag_category",
    "urg_diag_category",
    "dentmo",
    "denta",
    # "dentdow",
    "dentj",
    "denttod",
    "hopital",
    "typeauto",
    "umano",
    "sexe",
    "modent",
    "Congestive heart failure",
    "Cardiac arrhythmias",
    "Valvular disease",
    "Pulmonary circulation disorders",
    "Peripheral vascular disorders",
    "Hypertension uncomplicated",
    "Hypertension complicated",
    "Paralysis",
    "Other neurological disorders",
    "Chronic pulmonary disease",
    "Diabetes uncomplicated",
    "Diabetes complicated",
    "Hypothyroidism",
    "Renal failure",
    "Liver disease",
    "Metastatic cancer",
    "Solid tumour without metastasis",
    "Rheumatoid arthritis/collagen vascular diseases",
    "Coagulopathy",
    "Obesity",
    "Weight loss",
    "Fluid and electrolyte disorders",
    "Blood loss anaemia",
    "Deficiency anaemia",
    "Alcohol abuse",
    "Depression",
    "AAFA001",
    "AAFA002",
    "AAFA007",
    "AALA004",
    "AALB001",
    "AALB002",
    "ABCB001",
    "ABLB003",
    "ACFA002",
    "ACFA007",
    "ACFA013",
    "ACFA028",
    "ACPA001",
    "AFLB008",
    "AGLB001",
    "DBKA006",
    "DBMA002",
    "DDAF008",
    "DDMA031",
    "DDQH009",
    "DENF018",
    "DEPF033",
    "DEQF004",
    "DZEA002",
    "EASF010",
    "EBFA008",
    "EBLA003",
    "EPLF005",
    "EQLF004",
    "EQQF004",
    "GEJE001",
    "GEQE004",
    "GEQE007",
    "GEQE009",
    "GEQE012",
    "HEQE002",
    "HHFA016",
    "HJQE001",
    "HPFA003",
    "HPFA004",
    "JAEA003",
    "JQGA002",
    "JQGA003",
    "JQGA004",
    "JVJF002",
    "JVJF004",
    "JVJF005",
    "JVJF008",
    "KCFA005",
    "NBCA006",
    "NBCA010",
    "NEKA020",
    "ZCQA001",
    "dureeurg",
    "ccmu",
    "igs2",
    "cmuc",
    "went_to_emergency_care",
    "RUMNU",
]

model = los(
    layers=[512, 256, 192, 128, 64, 32],
    emb_sz_factor=7,
    use_bn=False,
    ps=0,
    emb_drop=0,
    # emb_szs=len(ordinal_cols),
    emb_szs=[len(cats) for cats in ord_enc.categories_],
    n_cont=len(cont_cols),
    out_sz=len(label_enc.classes_),
)

fig, (ax1, ax2, ax3) = plt.subplots(
    nrows=3,
    figsize=(12, 20),
    # For horizontal layout
    # figsize=(40, 6),
)

y_trues, y_predss = [], []
for fold in range(1, 6):

    checkpoint = torch.load("model_fold_" + str(fold))

    model.load_state_dict(checkpoint["model"])
    x_tst_fold = x_df.iloc[checkpoint["test_idx"]]
    y_tst_fold = y_df.iloc[checkpoint["test_idx"]]
    tst = torch.utils.data.TensorDataset(
        torch.Tensor(x_tst_fold.values), torch.LongTensor(y_tst_fold.values.squeeze())
    )
    tst_loader = torch.utils.data.DataLoader(tst, batch_size=1024, shuffle=False)

    y_true, y_preds = get_preds(model, tst_loader, ordinal_cols, label_enc)
    y_true, y_preds = label_enc.transform(y_true), label_enc.transform(y_preds)
    y_trues.append(y_true)
    y_predss.append(y_preds)

y_trues = np.concatenate(y_trues, axis=0, dtype="float")
y_predss = np.concatenate(y_predss, axis=0, dtype="float")
bland_altman_plot(y_trues, y_predss, ax1, fig)

cont_cols = (
    [
        "age",
        "mean_los_um_n_days",
        "mean_los_diag_n_days",
        "mean_los_n_days",
        "n_adm_n_days",
        "n_adm_n_days_diag",
        "n_adm_n_days_um",
        "n_adm_n_days_patient",
        "time_already_spent_stay_patient",
        # "time_already_spent_n_days_patient",
        "Number of elixhauser commorbidities",
        "Number of commorbidities",
        "n_acts",
        "urg_n_commorbidities",
        "urg_n_acts",
        "urg_adm_n_days",
        "urg_n_adm_n_days_diag",
        "urg_n_adm_n_days_patient",
    ]
    + ["mean_los_diag_cat_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
    + ["mean_los_diag_cat_um_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
)

ordinal_cols = [
    "diag_category",
    "urg_diag_category",
    "dentmo",
    "denta",
    "dentdow",
    "denttod",
    "hopital",
    "typeauto",
    "umano",
    "sexe",
    "modent",
    "Congestive heart failure",
    "Cardiac arrhythmias",
    "Valvular disease",
    "Pulmonary circulation disorders",
    "Peripheral vascular disorders",
    "Hypertension uncomplicated",
    "Hypertension complicated",
    "Paralysis",
    "Other neurological disorders",
    "Chronic pulmonary disease",
    "Diabetes uncomplicated",
    "Diabetes complicated",
    "Hypothyroidism",
    "Renal failure",
    "Liver disease",
    "Metastatic cancer",
    "Solid tumour without metastasis",
    "Rheumatoid arthritis/collagen vascular diseases",
    "Coagulopathy",
    "Obesity",
    "Weight loss",
    "Fluid and electrolyte disorders",
    "Blood loss anaemia",
    "Deficiency anaemia",
    "Alcohol abuse",
    "Depression",
    "AAFA001",
    "AAFA002",
    "AAFA007",
    "AALA004",
    "AALB001",
    "AALB002",
    "ABCB001",
    "ABLB003",
    "ACFA002",
    "ACFA007",
    "ACFA013",
    "ACFA028",
    "ACPA001",
    "AFLB008",
    "AGLB001",
    "DBKA006",
    "DBMA002",
    "DDAF008",
    "DDMA031",
    "DDQH009",
    "DENF018",
    "DEPF033",
    "DEQF004",
    "DZEA002",
    "EASF010",
    "EBFA008",
    "EBLA003",
    "EPLF005",
    "EQLF004",
    "EQQF004",
    "GEJE001",
    "GEQE004",
    "GEQE007",
    "GEQE009",
    "GEQE012",
    "HEQE002",
    "HHFA016",
    "HJQE001",
    "HPFA003",
    "HPFA004",
    "JAEA003",
    "JQGA002",
    "JQGA003",
    "JQGA004",
    "JVJF002",
    "JVJF004",
    "JVJF005",
    "JVJF008",
    "KCFA005",
    "NBCA006",
    "NBCA010",
    "NEKA020",
    "ZCQA001",
    "dureeurg",
    "ccmu",
    "igs2",
    "cmuc",
    "went_to_emergency_care",
    "RUMNU",
]
x_df, y_df, ordinal_enc, label_enc = dataset.format_dataset_ml(
    "../notebooks/pmsi_full.csv", ordinal_cols, cont_cols
)


if not exists("model_ml_1"):
    print("ml models not found, training...")
    models_ml = train_ml_bland_altman(x_df, y_df)
    for fold in range(1, 6):
        with open("model_ml_" + str(fold), "wb") as fh:
            pickle.dump(models_ml[fold - 1], fh)
    print("ml models saved")
else:
    print("loading ml models")
    models_ml = []
    for fold in range(1, 6):
        print("loading model " + str(fold))
        with open("model_ml_" + str(fold), "rb") as fh:
            models_ml.append(pickle.load(fh))


y_trues, y_predss = [], []
for fold, (trn_index, tst_index) in enumerate(
    KFold(n_splits=5, random_state=42, shuffle=True).split(x_df, y_df), start=1
):
    print("fold " + str(fold))
    model = models_ml[fold - 1]
    x_tst_fold = x_df.iloc[tst_index]
    y_tst_fold = y_df.iloc[tst_index]
    y_trues.append(y_tst_fold)
    y_predss.append(model.predict(x_tst_fold))

y_trues = np.concatenate(y_trues, axis=0, dtype="float")
y_predss = np.concatenate(y_predss, axis=0, dtype="float")
bland_altman_plot(y_trues, y_predss, ax2, fig)
ax2.set_title("Random forest")
del models_ml
print("done machine learning")


ordinal_cols = [
    # faster
    # "diag_category",
    # "urg_diag_category",
    "dentmo",
    "denta",
    # "dentdow",
    "dentj",
    "denttod",
    "hopital",
    "typeauto",
    "umano",
    "sexe",
    "modent",
    "Congestive heart failure",
    "Cardiac arrhythmias",
    "Valvular disease",
    "Pulmonary circulation disorders",
    "Peripheral vascular disorders",
    "Hypertension uncomplicated",
    "Hypertension complicated",
    "Paralysis",
    "Other neurological disorders",
    "Chronic pulmonary disease",
    "Diabetes uncomplicated",
    "Diabetes complicated",
    "Hypothyroidism",
    "Renal failure",
    "Liver disease",
    "Metastatic cancer",
    "Solid tumour without metastasis",
    "Rheumatoid arthritis/collagen vascular diseases",
    "Coagulopathy",
    "Obesity",
    "Weight loss",
    "Fluid and electrolyte disorders",
    "Blood loss anaemia",
    "Deficiency anaemia",
    "Alcohol abuse",
    "Depression",
    "AAFA001",
    "AAFA002",
    "AAFA007",
    "AALA004",
    "AALB001",
    "AALB002",
    "ABCB001",
    "ABLB003",
    "ACFA002",
    "ACFA007",
    "ACFA013",
    "ACFA028",
    "ACPA001",
    "AFLB008",
    "AGLB001",
    "DBKA006",
    "DBMA002",
    "DDAF008",
    "DDMA031",
    "DDQH009",
    "DENF018",
    "DEPF033",
    "DEQF004",
    "DZEA002",
    "EASF010",
    "EBFA008",
    "EBLA003",
    "EPLF005",
    "EQLF004",
    "EQQF004",
    "GEJE001",
    "GEQE004",
    "GEQE007",
    "GEQE009",
    "GEQE012",
    "HEQE002",
    "HHFA016",
    "HJQE001",
    "HPFA003",
    "HPFA004",
    "JAEA003",
    "JQGA002",
    "JQGA003",
    "JQGA004",
    "JVJF002",
    "JVJF004",
    "JVJF005",
    "JVJF008",
    "KCFA005",
    "NBCA006",
    "NBCA010",
    "NEKA020",
    "ZCQA001",
    "dureeurg",
    "ccmu",
    "igs2",
    "cmuc",
    "went_to_emergency_care",
    "RUMNU",
]


cont_cols = (
    [
        "age",
        "mean_los_um_n_days",
        "mean_los_diag_n_days",
        "mean_los_n_days",
        "n_adm_n_days",
        "n_adm_n_days_diag",
        "n_adm_n_days_um",
        "n_adm_n_days_patient",
        "time_already_spent_stay_patient",
        # "time_already_spent_n_days_patient",
        "Number of elixhauser commorbidities",
        "Number of commorbidities",
        "n_acts",
        "urg_n_commorbidities",
        "urg_n_acts",
        "urg_adm_n_days",
        "urg_n_adm_n_days_diag",
        "urg_n_adm_n_days_patient",
    ]
    + ["mean_los_diag_cat_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
    + ["mean_los_diag_cat_um_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
)

x_df, y_df, ordinal_enc, label_enc = dataset.format_dataset_reg(
    "../notebooks/pmsi_full.csv", ordinal_cols, cont_cols
)

if not exists("model_reg_1"):
    print("reg models not found, training...")
    models_reg = train_reg_log_bland_altman(x_df, y_df)
    for fold in range(1, 6):
        with open("model_reg_" + str(fold), "wb") as fh:
            pickle.dump(models_reg[fold - 1], fh)
    print("reg models saved")
else:
    print("loading reg models")
    models_reg = []
    for fold in range(1, 6):
        with open("model_reg_" + str(fold), "rb") as fh:
            models_reg.append(pickle.load(fh))

y_trues, y_predss = [], []
for fold, (trn_index, tst_index) in enumerate(
    KFold(n_splits=5, random_state=42, shuffle=True).split(x_df, y_df), start=1
):
    model = models_reg[fold - 1]
    x_tst_fold = x_df.iloc[tst_index]
    y_tst_fold = y_df.iloc[tst_index]
    y_trues.append(y_tst_fold)
    y_predss.append(model.predict(x_tst_fold))

y_trues = np.concatenate(y_trues, axis=0, dtype="float")
y_predss = np.concatenate(y_predss, axis=0, dtype="float")

bland_altman_plot(y_trues, y_predss, ax3, fig)
ax3.set_title("Logistic regression")
del models_reg
plt.tight_layout()
fig.suptitle(
    "Bland-Altman plot for the FFNN, random forest and logistic regression models",
    fontsize=16,
)

for n, ax in enumerate([ax1, ax2, ax3]):

    # ax.imshow(np.random.randn(10,10), interpolation='none')
    ax.text(
        -0.02,
        1.03,
        "(" + string.ascii_uppercase[n] + ")",
        transform=ax.transAxes,
        size=11,
        weight="bold",
    )
fig.subplots_adjust(top=0.95)
fig.savefig("bland_altman.png", dpi=300)
fig.savefig("bland_altman.eps", dpi=300)
fig.savefig("bland_altman.pdf", dpi=300)
