# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import dataset
from train import train_ml, train_ml_cm
import pickle
import numpy as np
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.metrics import ConfusionMatrixDisplay
import matplotlib.pyplot as plt

start = datetime.now()


one_hot_cols = []

ordinal_cols = [
    "diag_category",
    "urg_diag_category",
    "dentmo",
    "denta",
    # "dentdow",
    "dentj",
    "denttod",
    "hopital",
    "typeauto",
    "umano",
    "sexe",
    "modent",
    "Congestive heart failure",
    "Cardiac arrhythmias",
    "Valvular disease",
    "Pulmonary circulation disorders",
    "Peripheral vascular disorders",
    "Hypertension uncomplicated",
    "Hypertension complicated",
    "Paralysis",
    "Other neurological disorders",
    "Chronic pulmonary disease",
    "Diabetes uncomplicated",
    "Diabetes complicated",
    "Hypothyroidism",
    "Renal failure",
    "Liver disease",
    "Metastatic cancer",
    "Solid tumour without metastasis",
    "Rheumatoid arthritis/collagen vascular diseases",
    "Coagulopathy",
    "Obesity",
    "Weight loss",
    "Fluid and electrolyte disorders",
    "Blood loss anaemia",
    "Deficiency anaemia",
    "Alcohol abuse",
    "Depression",
    "AAFA001",
    "AAFA002",
    "AAFA007",
    "AALA004",
    "AALB001",
    "AALB002",
    "ABCB001",
    "ABLB003",
    "ACFA002",
    "ACFA007",
    "ACFA013",
    "ACFA028",
    "ACPA001",
    "AFLB008",
    "AGLB001",
    "DBKA006",
    "DBMA002",
    "DDAF008",
    "DDMA031",
    "DDQH009",
    "DENF018",
    "DEPF033",
    "DEQF004",
    "DZEA002",
    "EASF010",
    "EBFA008",
    "EBLA003",
    "EPLF005",
    "EQLF004",
    "EQQF004",
    "GEJE001",
    "GEQE004",
    "GEQE007",
    "GEQE009",
    "GEQE012",
    "HEQE002",
    "HHFA016",
    "HJQE001",
    "HPFA003",
    "HPFA004",
    "JAEA003",
    "JQGA002",
    "JQGA003",
    "JQGA004",
    "JVJF002",
    "JVJF004",
    "JVJF005",
    "JVJF008",
    "KCFA005",
    "NBCA006",
    "NBCA010",
    "NEKA020",
    "ZCQA001",
    "dureeurg",
    "ccmu",
    "igs2",
    "cmuc",
    "went_to_emergency_care",
    "RUMNU",
]

emb_cols = ordinal_cols + one_hot_cols

cont_cols = (
    [
        "age",
        "mean_los_um_n_days",
        "mean_los_diag_n_days",
        "mean_los_n_days",
        "n_adm_n_days",
        "n_adm_n_days_diag",
        "n_adm_n_days_um",
        "n_adm_n_days_patient",
        "time_already_spent_stay_patient",
        # "time_already_spent_n_days_patient",
        "Number of elixhauser commorbidities",
        "Number of commorbidities",
        "n_acts",
        "urg_n_commorbidities",
        "urg_n_acts",
        "urg_adm_n_days",
        "urg_n_adm_n_days_diag",
        "urg_n_adm_n_days_patient",
    ]
    + ["mean_los_diag_cat_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
    + ["mean_los_diag_cat_um_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
)

x_df, y_df, ordinal_enc, label_enc = dataset.format_dataset_ml(
    "../notebooks/pmsi_full.csv", ordinal_cols, cont_cols
)
print(list(x_df.columns))

# save for eval afterward
x_df.to_csv("x_df_to_eval_ml.csv")
y_df.to_csv("y_df_to_eval_ml.csv")
with open("ord_enc_to_eval_ml", "wb") as fh:
    pickle.dump(ordinal_enc, fh)
with open("label_enc_to_eval_ml", "wb") as fh:
    pickle.dump(label_enc, fh)


model = train_ml(x_df, y_df)
with open("model_ml", "wb") as fh:
    pickle.dump(model, fh)


end = datetime.now()
print("Training took {}".format(end - start))

# Create a confusion matrix
(
    x_trn_fold,
    x_tst_fold,
    y_trn_fold,
    y_tst_fold,
) = train_test_split(x_df, y_df, test_size=0.2, shuffle=False)
cm = train_ml_cm(x_trn_fold, x_tst_fold, y_trn_fold, y_tst_fold, label_enc)
classes = list(range(0, 14)) + ["14+"]
disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=classes)
fig, ax = plt.subplots(1, 1, figsize=(16, 16))
disp.plot(ax=ax, cmap="Blues")  # , values_format=".1%")
classes = (
    ["0 days", "1 day"] + [str(day) + " days" for day in range(2, 14)] + ["14+ days"]
)

classes = [
    day
    + " ("
    + str(
        "{:,}".format(
            y_tst_fold.value_counts()[int(day.split(" ")[0].replace("+", ""))]
        )
    ).replace(",", " ")
    + " RUM)"
    for day in classes
]

ax.set_yticklabels(classes)
ax.set_title("Confusion matrix with test set size: " + str(len(y_tst_fold)))
disp.figure_.savefig("cm_ml.eps", bbox_inches="tight", dpi=300)
disp.figure_.savefig("cm_ml.png", bbox_inches="tight", dpi=300)
