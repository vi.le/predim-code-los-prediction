# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import dataset
from model import los
from evaluate import evaluate
import pickle
import numpy as np
from datetime import datetime
from bst import LOSBST
from copy import deepcopy
import json
import time
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.utils.class_weight import compute_class_weight
import torch
from torchinfo import summary


one_hot_cols = []

ordinal_cols = [
    "diag_category",
    "urg_diag_category",
    "dentmo",
    "denta",
    # "dentdow",
    "dentj",
    "denttod",
    "hopital",
    "typeauto",
    "umano",
    "sexe",
    "modent",
    "Congestive heart failure",
    "Cardiac arrhythmias",
    "Valvular disease",
    "Pulmonary circulation disorders",
    "Peripheral vascular disorders",
    "Hypertension uncomplicated",
    "Hypertension complicated",
    "Paralysis",
    "Other neurological disorders",
    "Chronic pulmonary disease",
    "Diabetes uncomplicated",
    "Diabetes complicated",
    "Hypothyroidism",
    "Renal failure",
    "Liver disease",
    "Metastatic cancer",
    "Solid tumour without metastasis",
    "Rheumatoid arthritis/collagen vascular diseases",
    "Coagulopathy",
    "Obesity",
    "Weight loss",
    "Fluid and electrolyte disorders",
    "Blood loss anaemia",
    "Deficiency anaemia",
    "Alcohol abuse",
    "Depression",
    "AAFA001",
    "AAFA002",
    "AAFA007",
    "AALA004",
    "AALB001",
    "AALB002",
    "ABCB001",
    "ABLB003",
    "ACFA002",
    "ACFA007",
    "ACFA013",
    "ACFA028",
    "ACPA001",
    "AFLB008",
    "AGLB001",
    "DBKA006",
    "DBMA002",
    "DDAF008",
    "DDMA031",
    "DDQH009",
    "DENF018",
    "DEPF033",
    "DEQF004",
    "DZEA002",
    "EASF010",
    "EBFA008",
    "EBLA003",
    "EPLF005",
    "EQLF004",
    "EQQF004",
    "GEJE001",
    "GEQE004",
    "GEQE007",
    "GEQE009",
    "GEQE012",
    "HEQE002",
    "HHFA016",
    "HJQE001",
    "HPFA003",
    "HPFA004",
    "JAEA003",
    "JQGA002",
    "JQGA003",
    "JQGA004",
    "JVJF002",
    "JVJF004",
    "JVJF005",
    "JVJF008",
    "KCFA005",
    "NBCA006",
    "NBCA010",
    "NEKA020",
    "ZCQA001",
    "dureeurg",
    "ccmu",
    "igs2",
    "cmuc",
    "went_to_emergency_care",
    "RUMNU",
]

emb_cols = ordinal_cols + one_hot_cols

cont_cols = [
    "age",
    "mean_los_um_n_days",
    "mean_los_diag_n_days",
    "mean_los_n_days",
    "n_adm_n_days",
    "n_adm_n_days_diag",
    "n_adm_n_days_um",
    "n_adm_n_days_patient",
    "time_already_spent_stay_patient",
    # "time_already_spent_n_days_patient",
    "Number of elixhauser commorbidities",
    "Number of commorbidities",
    "n_acts",
    "urg_n_commorbidities",
    "urg_n_acts",
    "urg_adm_n_days",
    "urg_n_adm_n_days_diag",
    "urg_n_adm_n_days_patient",
] + ["mean_los_diag_cat_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]


class Tree:
    def __init__(self, n_days: int, graph=None, balanced=False):
        (
            x_df,
            y_df,
            self.cat_enc,
            self.label_enc,
            self.cont_enc,
        ) = dataset.format_dataset_dl(
            "../notebooks/pmsi_full.csv",
            ordinal_cols,
            cont_cols,
        )

        self.n_days = n_days

        # If not balanced, we need the classifier for -n_days + n_days, Thus we
        # make sure to include n_days + 1 for the last node. It will not be
        # included because the leaves are always the ones on the left
        if not balanced:
            self.n_days += 1
        # Get the days so we can compute the median and build the tree
        days = sorted(y_df.values)

        self.cv = 5
        self.balanced = balanced

        if graph:
            self._init_graph(days, self.n_days)
            self.los_graph.G = self.load_graph(graph)
            self.test_overall()
            self._colorize_graph()
            self._relabel_graph()
        else:
            # Load training data
            print("training on {} RUM".format(len(x_df)))
            print("training on {} patients".format(len(x_df["ippano"].unique())))
            print("training on {} stays".format(len(x_df["rssnuano"].unique())))

            # Remove los_cont since it's no longer
            x_df.drop(
                [
                    "dentdow",
                    "level_0",
                    "Unnamed: 0",
                    "los_cont",
                    "rssnuano",
                    "ippano",
                ],
                axis=1,
                inplace=True,
            )
            print(list(x_df.columns))

            # temp will use k-CV after
            x_df = x_df.reindex(columns=ordinal_cols + cont_cols, copy=False)
            (
                self.x_trn_fold,
                self.x_tst_fold,
                self.y_trn_fold,
                self.y_tst_fold,
            ) = train_test_split(x_df, y_df, test_size=0.2, shuffle=False)
            self.test_idx = list(self.y_tst_fold.index)

            # save for eval afterward
            x_df.to_csv("x_df_to_eval_graph.csv")
            y_df.to_csv("y_df_to_eval_graph.csv")
            with open("ord_enc_to_eval_graph", "wb") as fh:
                pickle.dump(self.cat_enc, fh)
            with open("label_enc_to_eval_graph", "wb") as fh:
                pickle.dump(self.label_enc, fh)
            with open("cont_enc_to_eval_graph", "wb") as fh:
                pickle.dump(self.cont_enc, fh)

            self._init_graph(days, self.n_days)
            self.los_graph.draw()
            # Train a NN and ML model per node in the tree. We decide
            # which one is the best later on
            self.train_accuracy = self._init_models(self.models)
            # Assign the models to the nodes in the tree
            self._create_model_graph()
            self.save_graph()
            # Used at prediction time to convert the categories to ids
            self.create_confusion_matrices()

    def test_overall(self):
        print(self.los_graph.G.nodes)
        # get the test_ixd
        test_idx = torch.load("model_node_-2 2")["test_idx"]
        x_df = pd.read_csv("x_df_to_eval_graph.csv")
        y_df = pd.read_csv("y_df_to_eval_graph.csv")
        with open("ord_enc_to_eval_graph", "rb") as fh:
            ord_enc = pickle.load(fh)
        with open("label_enc_to_eval_graph", "rb") as fh:
            label_enc = pickle.load(fh)
        with open("cont_enc_to_eval_graph", "rb") as fh:
            cont_enc = pickle.load(fh)

        x_df.drop(
            [
                "Unnamed: 0",
            ],
            axis=1,
            inplace=True,
        )

        y_df.drop(
            [
                "Unnamed: 0",
            ],
            axis=1,
            inplace=True,
        )
        x_tst_fold = x_df.iloc[test_idx]
        y_tst_fold = y_df.iloc[test_idx]
        tst = torch.utils.data.TensorDataset(
            torch.Tensor(x_tst_fold.values),
            torch.LongTensor(y_tst_fold.values.squeeze()),
        )
        tst_loader = torch.utils.data.DataLoader(tst, batch_size=1024, shuffle=False)
        total = 0
        correct = 0
        for (x, y) in tst_loader:
            preds = []
            for (x_i, y_i) in zip(x, y):
                preds.append(self.predict(x_i, "root"))
            total += x.size(0)
            correct += (torch.Tensor(preds) == y).sum().item()
            print(correct / total)
        print(correct / total)

    def _init_graph(self, days, n_days: int):
        self.los_graph = LOSBST(days, self.n_days, self.balanced)
        # Get the models to train
        self.models = self.los_graph.get_path()
        # Used to find the immediate parent before the node
        self.parent_name = {
            " ".join(model["name"]): str(model["parents"][-1])
            if model["parents"]
            else "root"
            for model in self.models
        }
        # Find the nodes right after the model
        self.model_name = {v: k.split(" ") for k, v in self.parent_name.items()}
        self.models_ml = {" ".join(model["name"]): None for model in self.models}
        self.models_nn = {" ".join(model["name"]): None for model in self.models}

    def _fine_tune(self, bounds, parents, x, y):
        """Update the dataset according to where we are in the tree"""
        if not bounds[0].startswith("j"):
            lower_bound = abs(int(bounds[0]))
            upper_bound = int(bounds[1])
            y.loc[y < lower_bound] = 0
            y.loc[y >= upper_bound] = 1

        else:
            lower_bound = int(bounds[0][1:])
            upper_bound = int(bounds[1][1:])
            idx = y.index[(y == lower_bound) | (y == upper_bound)]
            x = x.iloc[idx]
            y = y.iloc[idx]
            y.loc[
                y == lower_bound,
            ] = 0

            y.loc[
                y == upper_bound,
            ] = 1

        return x, y

    def _balance_data(self):
        feature_cols = list(
            filter(lambda column: column != "time_in_hospital", self.data.columns)
        )
        sm = RandomUnderSampler(random_state=42)
        X = self.data.loc[:, feature_cols]
        Y = self.data.time_in_hospital
        X_res, Y_res = sm.fit_resample(X, Y)
        X_res = pd.DataFrame(X_res, columns=feature_cols)
        for col in feature_cols:
            X_res[col] = X_res[col].astype("category")

        self.data = pd.concat(
            [X_res, pd.DataFrame(Y_res, columns=["time_in_hospital"])], axis=1
        )
        self._update_counts()

    def _init_models(self, models_names: list) -> dict:
        """Create one NN and ML model"""
        train_accuracy = {}

        for model in self.models:
            print("Training model for " + str(model["name"]))
            # reset the index to make sure they match between x and y after
            # train test split
            model_dataset = deepcopy(self.x_trn_fold).reset_index(drop=True)
            model_dataset_y = deepcopy(self.y_trn_fold).reset_index(drop=True)
            model_test_dataset = deepcopy(self.x_tst_fold).reset_index(drop=True)
            model_test_dataset_y = deepcopy(self.y_tst_fold).reset_index(drop=True)
            # Relabel the dataset according to where we are in the tree. So the
            # variance of the sets are reduced
            model_dataset, model_dataset_y = self._fine_tune(
                model["name"], model["parents"], model_dataset, model_dataset_y
            )
            trn = torch.utils.data.TensorDataset(
                torch.Tensor(model_dataset.values),
                torch.LongTensor(model_dataset_y.values),
            )
            trn_loader = torch.utils.data.DataLoader(
                trn, batch_size=512, shuffle=True, num_workers=1
            )
            model_test_dataset, model_test_dataset_y = self._fine_tune(
                model["name"],
                model["parents"],
                model_test_dataset,
                model_test_dataset_y,
            )
            tst = torch.utils.data.TensorDataset(
                torch.Tensor(model_test_dataset.values),
                torch.LongTensor(model_test_dataset_y.values),
            )
            tst_loader = torch.utils.data.DataLoader(
                tst, batch_size=1024, shuffle=False
            )

            self.iters = len(trn_loader)
            self.weights = compute_class_weight(
                "balanced",
                classes=np.unique(model_dataset_y.values),
                y=model_dataset_y.values,
            )
            self.models_nn[" ".join(model["name"])], acc = self._train_nn(
                trn_loader, tst_loader, " ".join(model["name"])
            )

            # These things are huge, make sure they get GC'ed asap
            del model_dataset
            del model_test_dataset

            train_accuracy[" ".join(model["name"])] = {
                "nn_acc": acc,
                "ml_acc": 0,
            }

        return train_accuracy

    def _train_nn(self, trn_loader, tst_loader, node):
        """
        Train a neural network and return both the best model and its
        performance measure value

        """
        model = los(
            emb_szs=[len(cats) for cats in self.cat_enc.categories_],
            n_cont=len(cont_cols),
            # out_sz=len(self.label_enc.classes_),
            # Every model is binary in bst
            out_sz=2,
            layers=[512, 256, 192, 128, 64, 32],
            emb_drop=0,
            emb_sz_factor=7,
            use_bn=False,
            ps=0,
        )
        summary(
            model,
            input_size=(512, [len(cats) for cats in self.cat_enc.categories_]),
        )
        print(self.weights)
        criterion = torch.nn.NLLLoss(weight=torch.Tensor(self.weights))
        # criterion = torch.nn.NLLLoss()
        optimizer = torch.optim.AdamW(
            model.parameters(),
            lr=0.001,
            weight_decay=0.001,
        )
        # scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
        #    optimizer, T_0=4, T_mult=2, eta_min=1e-5
        # )

        tst_acc_arr = []
        tst_acc = evaluate(model, tst_loader, ordinal_cols)
        tst_acc_arr.append([0, tst_acc[0], tst_acc[1]])
        for epoch in range(0, 50):
            print("Epoch: " + str(epoch + 1))
            for idx, (x, y) in enumerate(trn_loader, start=1):
                # For the embedings to work it should be long values
                x_cat = x[:, : len(ordinal_cols)].long()
                x_cont = x[:, len(ordinal_cols) :]
                optimizer.zero_grad()
                output = model(x_cat, x_cont)
                loss = criterion(output, y)
                loss.backward()
                optimizer.step()
                # scheduler.step(epoch + idx / iters)
            model.eval()
            with torch.no_grad():
                tst_acc = evaluate(model, tst_loader, ordinal_cols)
            model.train()
            print("Test acc: " + str(tst_acc))
            tst_acc_arr.append([epoch + 1, tst_acc[0], tst_acc[1]])

        with open("node_" + str(node) + "_tst.json", "w") as fh:
            json.dump(tst_acc_arr, fh)
        torch.save(
            {"model": model.state_dict(), "test_idx": self.test_idx},
            "model_node_" + str(node),
        )

        # Return the model and last accuracy
        return model, tst_acc_arr[-1][1]

    def _create_model_graph(self):
        """Map the models to the graph"""
        for name, model in self.train_accuracy.items():
            # Get the parent above the model
            node_name = self.parent_name[name]
            # Which model was the best?
            if model["nn_acc"] > model["ml_acc"]:
                self.los_graph.G.nodes[node_name]["model"] = self.models_nn[name]
                self.los_graph.G.nodes[node_name]["kind"] = "nn"
                self.los_graph.G.nodes[node_name]["weight"] = model["nn_acc"]
            else:
                self.los_graph.G.nodes[node_name]["kind"] = "ml"
                self.los_graph.G.nodes[node_name]["model"] = self.models_ml[name]
                self.los_graph.G.nodes[node_name]["weight"] = model["ml_acc"]

    def save_graph(self):
        # The path is a windows path as we use the windows python interpreter
        with open("model_graph_" + str(int(time.time())) + ".gpickle", "wb") as fh:
            nx.write_gpickle(self.los_graph.G, fh)

    @staticmethod
    def load_graph(file_name: str):
        """Load a trained graph"""
        return nx.read_gpickle(file_name)

    def predict(self, x, n="root"):
        if n.startswith("j"):
            # Remove the "j" and return the number
            return int(n[1:])
        G = self.los_graph.G
        # if the node has only one child because of elimination of lower and
        # upper, return the child
        if G.out_degree(n) == 1:
            return int(list(self.los_graph.G.successors(n))[0][1:])
        x_cat = x[: len(ordinal_cols)].long()
        x_cont = x[len(ordinal_cols) :]
        # feed the data to the model at the node n
        pred = self.los_graph.G.nodes(data=True)[n]["model"](x_cat, x_cont).argmax(
            dim=0
        )
        # Left. This assumes the node in the tree are correctly ordored
        if pred.item() == 0:
            return self.predict(x, n=list(self.los_graph.G.successors(n))[0])
        # Right
        else:
            return self.predict(x, n=list(self.los_graph.G.successors(n))[1])

    def _colorize_graph(self):
        """Colorize the node according to their weights and a label the weight
        and the model name"""
        # https://matplotlib.org/3.1.1/gallery/color/colormap_reference.html
        cmap = plt.cm.Greys_r
        # The weight ought to be between 0 and 1
        norm = matplotlib.colors.Normalize(vmin=0, vmax=1)
        for node in self.los_graph.G.nodes:
            if node in self.los_graph.G.nodes and node in self.model_name:
                model_nodes = self.model_name[node]
                # The xlabel is the weight and the formated model name
                label = (
                    str(round(self.los_graph.G.nodes[node]["weight"], 3))
                    + " "
                    + self._format_model(
                        self.los_graph.G.nodes[node],
                        self.los_graph.G.nodes[node]["kind"],
                    )
                )
                # Convert weight to hex color according to the color map
                color = matplotlib.colors.rgb2hex(
                    cmap(norm(self.los_graph.G.nodes[node]["weight"]))
                )
                for model_node in model_nodes:
                    self.los_graph.G.nodes[model_node]["fillcolor"] = color
                    self.los_graph.G.nodes[model_node]["style"] = "filled"
                    self.los_graph.G.nodes[model_node]["xlabel"] = label

    def _friendly_model_name(self, node: str) -> str:
        """
        if node is Nj -> N days
        if node is N > 0 -> ≥ N days
        if node is N < 0 -> < N days
        """
        return (
            "{} days".format(node[1])
            if node.startswith("j")
            else "&#8805; {} days".format(str(abs(int(node))))
            if int(node) > 0
            else "< {} days".format(str(abs(int(node))))
        )

    def _uglify_model_name(self, node: str) -> str:
        # Well the code itself is ugly for now
        node = node.replace("&#8805;", "")
        return "".join([n for n in node if n.isdigit()])

    def _relabel_graph(self):
        """Relabel the node for pretty printing"""
        mapping = {
            node: self._friendly_model_name(node)
            for node in self.los_graph.G.nodes
            if node != "root"
        }
        # If copy == False (in place relabeling), the order of the nodes
        # changes for reasons that are beyond me
        self.los_graph.G = nx.relabel_nodes(self.los_graph.G, mapping, copy=True)

    @staticmethod
    def _format_model(node, kind) -> str:
        """Print-friendly model name"""
        if kind == "ml":
            name = str(node["model"]).split("Classifier")[0]
            return "".join(c for c in name if c.isupper())
        else:
            return "nn"

    def export_data_converters(self) -> dict:
        """Get the integer encoding dictionaries. Will be used for prediction"""

        return {
            column
            + "2id": (
                getattr(self.dataset, column + "2id")
                if hasattr(self.dataset, column + "2id")
                else {value: value for value in self.dataset.data[column].unique()}
            )
            for column in self.dataset.data.columns
        }

    @staticmethod
    def save_dict(data: dict, name: str = "data"):
        """Save dict to disk. Use pickle because json.dump might not work for custom
        types e.g numpy's"""
        with open(name, "wb") as fh:
            pickle.dump(data, fh)

    def create_confusion_matrices(self):
        """Create CM for each node in the tree.
        To make sure the error rates is the same for all classes"""
        pass


if __name__ == "__main__":
    start = datetime.now()
    tree = Tree(n_days=14, balanced=True, graph="model_graph_1622344836.gpickle")
    end = datetime.now()
    print("Took {}".format(end - start))
    # dataset = Dataset_Loader('diabetes_data/diabetic_data.csv', train=True, n_days=8)
    # x, y = tree._get_x_y(dataset)
    # tree.predict(x.sample(1))
