# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

from datetime import datetime
import numpy as np
import torch
from sklearn.model_selection import KFold, cross_validate
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegressionCV, LogisticRegression
import scipy.stats as st
from model import los, los_n_emb, los_logit
from evaluate import evaluate
import json
from sklearn.utils.class_weight import compute_class_weight
from sklearn.metrics import (
    confusion_matrix,
    cohen_kappa_score,
    accuracy_score,
    make_scorer,
)
import matplotlib.pyplot as plt

plt.rcParams.update(
    {
        "text.usetex": True,
        "font.family": "sans-serif",
    }
)


def train_dl(x_df, y_df, cat_encoder, ordinal_cols, cont_cols, label_enc):
    torch.set_num_threads(15)
    folds_acc = []
    folds_kappa = []
    for fold, (trn_index, tst_index) in enumerate(
        KFold(n_splits=5).split(x_df, y_df), start=1
    ):
        print("Fold: " + str(fold))
        start = datetime.now()
        x_trn_fold = x_df.iloc[trn_index]
        x_tst_fold = x_df.iloc[tst_index]
        y_trn_fold = y_df.iloc[trn_index]
        y_tst_fold = y_df.iloc[tst_index]

        trn = torch.utils.data.TensorDataset(
            torch.Tensor(x_trn_fold.values), torch.LongTensor(y_trn_fold.values)
        )
        tst = torch.utils.data.TensorDataset(
            torch.Tensor(x_tst_fold.values), torch.LongTensor(y_tst_fold.values)
        )
        trn_loader = torch.utils.data.DataLoader(
            trn, batch_size=512, shuffle=True, num_workers=1
        )
        tst_loader = torch.utils.data.DataLoader(tst, batch_size=1024, shuffle=False)

        # Can be used for cycling the learning rate
        iters = len(trn_loader)
        # Reward the model more for classification of uncommon classes i.e.
        # longer stays
        weights = compute_class_weight(
            "balanced", classes=np.unique(y_trn_fold.values), y=y_trn_fold.values
        )

        # use values from hyperparameter tuning results
        model = los(
            emb_szs=[len(cats) for cats in cat_encoder.categories_],
            n_cont=len(cont_cols),
            out_sz=len(label_enc.classes_),
            layers=[512, 256, 192, 128, 64, 32],
            emb_drop=0,
            emb_sz_factor=7,
            use_bn=False,
            ps=0,
        )
        print(model)
        # summary(
        #    model, input_size=(512, [len(cats) for cats in cat_encoder.categories_])
        # )
        criterion = torch.nn.NLLLoss(weight=torch.Tensor(weights))
        # criterion = torch.nn.NLLLoss()
        optimizer = torch.optim.AdamW(
            model.parameters(),
            lr=0.001,
            weight_decay=0.001,
        )
        # scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
        #    optimizer, T_0=4, T_mult=2, eta_min=1e-5
        # )

        tst_acc_arr = []
        tst_kappa_arr = []
        tst_acc = evaluate(model, tst_loader, ordinal_cols)
        tst_acc_arr.append([0, tst_acc[0], tst_acc[0]])
        tst_kappa_arr.append([0, tst_acc[0], tst_acc[2]])
        for epoch in range(0, 50):
            print("Epoch: " + str(epoch + 1))
            for idx, (x, y) in enumerate(trn_loader, start=1):
                # For the embedings to work it should be long values
                x_cat = x[:, : len(ordinal_cols)].long()
                x_cont = x[:, len(ordinal_cols) :]
                optimizer.zero_grad()
                output = model(x_cat, x_cont)
                loss = criterion(output, y)
                loss.backward()
                optimizer.step()
                # scheduler.step(epoch + idx / iters)
            model.eval()
            with torch.no_grad():
                tst_acc = evaluate(model, tst_loader, ordinal_cols)
            model.train()
            print("Test acc: " + str(tst_acc))
            tst_acc_arr.append([epoch + 1, tst_acc[0], tst_acc[0]])
            tst_kappa_arr.append([epoch + 1, tst_acc[0], tst_acc[2]])

        end = datetime.now()
        print("Took {}".format(end - start))
        with open("fold_" + str(fold) + "_tst.json", "w") as fh:
            json.dump(tst_acc_arr, fh)
        torch.save(
            {"model": model.state_dict(), "test_idx": tst_index},
            "model_fold_" + str(fold),
        )
        folds_acc.append(tst_acc_arr[-1][2])
        folds_kappa.append(tst_kappa_arr[-1][2])
    folds_acc = np.array(folds_acc)
    folds_kappa = np.array(folds_kappa)
    ci = st.t.interval(
        alpha=0.95, df=len(folds_acc) - 1, loc=folds_acc.mean(), scale=st.sem(folds_acc)
    )
    print(ci)
    ci = st.t.interval(
        alpha=0.95,
        df=len(folds_kappa) - 1,
        loc=folds_kappa.mean(),
        scale=st.sem(folds_kappa),
    )
    print(ci)


def train_dl_binary(
    x_df, y_df, cat_encoder, ordinal_cols, cont_cols, label_enc, day: int = 3
):
    torch.set_num_threads(15)
    for fold, (trn_index, tst_index) in enumerate(
        KFold(n_splits=5).split(x_df, y_df), start=1
    ):
        print("Fold: " + str(fold))
        start = datetime.now()
        x_trn_fold = x_df.iloc[trn_index]
        x_tst_fold = x_df.iloc[tst_index]
        y_trn_fold = y_df.iloc[trn_index]
        y_tst_fold = y_df.iloc[tst_index]

        trn = torch.utils.data.TensorDataset(
            torch.Tensor(x_trn_fold.values), torch.LongTensor(y_trn_fold.values)
        )
        tst = torch.utils.data.TensorDataset(
            torch.Tensor(x_tst_fold.values), torch.LongTensor(y_tst_fold.values)
        )
        trn_loader = torch.utils.data.DataLoader(
            trn, batch_size=512, shuffle=True, num_workers=1
        )
        tst_loader = torch.utils.data.DataLoader(tst, batch_size=1024, shuffle=False)

        iters = len(trn_loader)
        weights = compute_class_weight(
            "balanced", classes=np.unique(y_trn_fold.values), y=y_trn_fold.values
        )

        model = los_logit(
            emb_szs=[len(cats) for cats in cat_encoder.categories_],
            n_cont=len(cont_cols),
            out_sz=len(label_enc.classes_),
            layers=[512, 256, 192, 128, 64, 32],
            emb_drop=0,
            emb_sz_factor=7,
            use_bn=False,
            ps=0,
        )
        print(model)
        # summary(
        #    model, input_size=(512, [len(cats) for cats in cat_encoder.categories_])
        # )
        criterion = torch.nn.CrossEntropyLoss(weight=torch.Tensor(weights))
        # criterion = torch.nn.CrossEntropyLoss()
        optimizer = torch.optim.AdamW(
            model.parameters(),
            lr=0.001,
            weight_decay=0.001,
        )
        # scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
        #    optimizer, T_0=4, T_mult=2, eta_min=1e-5
        # )

        for epoch in range(0, 50):
            print("Epoch: " + str(epoch + 1))
            for idx, (x, y) in enumerate(trn_loader, start=1):
                # For the embedings to work it should be long values
                x_cat = x[:, : len(ordinal_cols)].long()
                x_cont = x[:, len(ordinal_cols) :]
                optimizer.zero_grad()
                output = model(x_cat, x_cont)
                loss = criterion(output, y)
                loss.backward()
                optimizer.step()
                # scheduler.step(epoch + idx / iters)

        end = datetime.now()
        print("Took {}".format(end - start))
        torch.save(
            {"model": model.state_dict(), "test_idx": tst_index},
            "model_fold_" + str(fold) + "_day_" + str(day),
        )


def train_dl_n_emb(x_df, y_df, cat_encoder, ordinal_cols, cont_cols, label_enc):
    """The goal was to test the neural network without embeddings"""
    torch.set_num_threads(15)
    for fold, (trn_index, tst_index) in enumerate(
        KFold(n_splits=5).split(x_df, y_df), start=1
    ):
        print("Fold: " + str(fold))
        x_trn_fold = x_df.iloc[trn_index]
        x_tst_fold = x_df.iloc[tst_index]
        y_trn_fold = y_df.iloc[trn_index]
        y_tst_fold = y_df.iloc[tst_index]

        trn = torch.utils.data.TensorDataset(
            torch.Tensor(x_trn_fold.values), torch.LongTensor(y_trn_fold.values)
        )
        tst = torch.utils.data.TensorDataset(
            torch.Tensor(x_tst_fold.values), torch.LongTensor(y_tst_fold.values)
        )
        trn_loader = torch.utils.data.DataLoader(
            trn, batch_size=512, shuffle=True, num_workers=0
        )
        tst_loader = torch.utils.data.DataLoader(tst, batch_size=1024, shuffle=False)

        iters = len(trn_loader)

        model = los_n_emb(
            emb_szs=len(ordinal_cols),
            n_cont=len(cont_cols),
            out_sz=len(label_enc.classes_),
            layers=[512, 256, 192, 128, 64, 32],
            use_bn=False,
            ps=0,
        )
        print(model)
        criterion = torch.nn.NLLLoss()
        optimizer = torch.optim.AdamW(
            model.parameters(),
            lr=0.001,
            weight_decay=0.001,
        )
        # scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
        #    optimizer, T_0=4, T_mult=2, eta_min=1e-5
        # )

        tst_acc_arr = []
        tst_acc = evaluate(model, tst_loader, ordinal_cols)
        tst_acc_arr.append([0, tst_acc[0], tst_acc[1]])
        for epoch in range(0, 60):
            print("Epoch: " + str(epoch + 1))
            for idx, (x, y) in enumerate(trn_loader, start=1):
                # For the embedings to work it should be long values
                x_cat = x[:, : len(ordinal_cols)].long()
                x_cont = x[:, len(ordinal_cols) :]
                optimizer.zero_grad()
                output = model(x_cat, x_cont)
                loss = criterion(output, y)
                loss.backward()
                optimizer.step()
                # scheduler.step(epoch + idx / iters)
            model.eval()
            with torch.no_grad():
                tst_acc = evaluate(model, tst_loader, ordinal_cols)
            model.train()
            print("Test acc: " + str(tst_acc))
            tst_acc_arr.append([epoch + 1, tst_acc[0], tst_acc[1]])

        with open("fold_" + str(fold) + "_tst_n_emb.json", "w") as fh:
            json.dump(tst_acc_arr, fh)
        torch.save(
            {"model": model.state_dict(), "test_idx": tst_index},
            "model_fold_" + str(fold) + "_n_emb",
        )


def train_ml(x_df, y_df):
    model = RandomForestClassifier(
        n_estimators=600,
        bootstrap=True,
        class_weight="balanced",
        criterion="gini",
        min_samples_split=1e-3,
        # Takes a LOT of memory
        max_depth=50,
        max_features="sqrt",
        n_jobs=None,
    )

    results = cross_validate(
        model,
        x_df,
        y_df,
        cv=KFold(n_splits=5),
        verbose=True,
        scoring={
            "accuracy": make_scorer(accuracy_score),
            "kappa": make_scorer(cohen_kappa_score, weights="linear"),
        },
        # Use only one process to workaround RAM use
        n_jobs=None,
        return_estimator=True,
    )
    print(results)

    return model


def train_ml_bland_altman(x_df, y_df):
    model = RandomForestClassifier(
        n_estimators=600,
        bootstrap=True,
        class_weight="balanced",
        criterion="gini",
        min_samples_split=1e-3,
        # Takes a LOT of memory
        max_depth=50,
        max_features="sqrt",
        n_jobs=None,
    )

    results = cross_validate(
        model,
        x_df,
        y_df,
        cv=KFold(n_splits=5, random_state=42, shuffle=True),
        verbose=True,
        scoring={
            "accuracy": make_scorer(accuracy_score),
            "kappa": make_scorer(cohen_kappa_score, weights="linear"),
        },
        # Use only one process to workaround RAM use
        n_jobs=None,
        return_estimator=True,
    )

    return results["estimator"]


def train_ml_cm(x_trn_fold, x_tst_fold, y_trn_fold, y_tst_fold, label_enc):
    model = RandomForestClassifier(
        n_estimators=600,
        bootstrap=True,
        class_weight="balanced",
        criterion="gini",
        # Takes a LOT of memory
        max_depth=55,
        max_features="sqrt",
        n_jobs=None,
    )
    model.fit(x_trn_fold, y_trn_fold)
    classes = list(set(y_tst_fold))
    print(model.predict(x_tst_fold).shape)
    cm = confusion_matrix(
        label_enc.inverse_transform(y_tst_fold),
        label_enc.inverse_transform(model.predict(x_tst_fold)),
        labels=classes,
    )  # , normalize="true")
    cm = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]

    return cm


def train_reg_log_bland_altman(x_df, y_df):
    model = LogisticRegressionCV(
        class_weight="balanced",
        solver="saga",
        max_iter=500,
        tol=5e-4,
        multi_class="multinomial",
        n_jobs=5,
        verbose=1,
        Cs=5,
        cv=KFold(n_splits=5),
        penalty="l2",
        # scoring={
        #     "accuracy": make_scorer(accuracy_score),
        #     "kappa": make_scorer(cohen_kappa_score, weights="linear"),
        # },
        scoring=make_scorer(accuracy_score),
    )
    # model.fit(x_df, y_df)
    # print(model)
    # print("Cs", model.Cs_)
    # print("C ", model.C_)
    # print("scores", model.scores_)
    results = cross_validate(
        LogisticRegression(
            class_weight="balanced",
            solver="saga",
            max_iter=500,
            tol=5e-4,
            multi_class="multinomial",
            n_jobs=5,
            verbose=1,
            C=1e-2,
            penalty="l2",
        ),
        x_df,
        y_df,
        cv=KFold(n_splits=5, random_state=42, shuffle=True),
        verbose=2,
        scoring={
            "accuracy": make_scorer(accuracy_score),
            "kappa": make_scorer(cohen_kappa_score, weights="linear"),
        },
        n_jobs=5,
        return_estimator=True,
    )
    print(results)

    return results["estimator"]


def train_reg_log(x_df, y_df):
    model = LogisticRegressionCV(
        class_weight="balanced",
        solver="saga",
        max_iter=500,
        tol=5e-4,
        multi_class="multinomial",
        n_jobs=5,
        verbose=1,
        Cs=5,
        cv=KFold(n_splits=5),
        penalty="l2",
        # scoring={
        #     "accuracy": make_scorer(accuracy_score),
        #     "kappa": make_scorer(cohen_kappa_score, weights="linear"),
        # },
        scoring=make_scorer(accuracy_score),
    )
    # model.fit(x_df, y_df)
    # print(model)
    # print("Cs", model.Cs_)
    # print("C ", model.C_)
    # print("scores", model.scores_)
    results = cross_validate(
        LogisticRegression(
            class_weight="balanced",
            solver="saga",
            max_iter=500,
            tol=5e-4,
            multi_class="multinomial",
            n_jobs=5,
            verbose=1,
            C=1e-2,
            penalty="l2",
        ),
        x_df,
        y_df,
        cv=KFold(n_splits=5),
        verbose=2,
        scoring={
            "accuracy": make_scorer(accuracy_score),
            "kappa": make_scorer(cohen_kappa_score, weights="linear"),
        },
        n_jobs=5,
    )
    print(results)

    return model


def train_reg_log_cm(x_trn_fold, x_tst_fold, y_trn_fold, y_tst_fold, label_enc):
    model = LogisticRegression(
        class_weight="balanced",
        solver="saga",
        C=1e-2,
        tol=5e-4,
        verbose=1,
        max_iter=500,
        multi_class="multinomial",
        n_jobs=8,
    )
    model.fit(x_trn_fold, y_trn_fold)
    classes = list(range(0, 14)) + ["14+"]
    print(model.predict(x_tst_fold).shape)
    cm = confusion_matrix(
        label_enc.inverse_transform(y_tst_fold),
        label_enc.inverse_transform(model.predict(x_tst_fold)),
        labels=classes,
    )  # , normalize="true")
    cm = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]

    return cm
