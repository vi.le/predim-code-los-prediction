# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import dataset
from train import train_dl, train_dl_n_emb, train_dl_binary
import pickle
import numpy as np
from datetime import datetime

start = datetime.now()


# Holds columns that would need to be one-hot encoded
one_hot_cols = []

# Categorical columns that will be converted to embeddings
ordinal_cols = [
    "diag_category",
    "urg_diag_category",
    "dentmo",
    "denta",
    # "dentdow",
    "dentj",
    "denttod",
    "hopital",
    "typeauto",
    "umano",
    "sexe",
    "modent",
    "Congestive heart failure",
    "Cardiac arrhythmias",
    "Valvular disease",
    "Pulmonary circulation disorders",
    "Peripheral vascular disorders",
    "Hypertension uncomplicated",
    "Hypertension complicated",
    "Paralysis",
    "Other neurological disorders",
    "Chronic pulmonary disease",
    "Diabetes uncomplicated",
    "Diabetes complicated",
    "Hypothyroidism",
    "Renal failure",
    "Liver disease",
    "Metastatic cancer",
    "Solid tumour without metastasis",
    "Rheumatoid arthritis/collagen vascular diseases",
    "Coagulopathy",
    "Obesity",
    "Weight loss",
    "Fluid and electrolyte disorders",
    "Blood loss anaemia",
    "Deficiency anaemia",
    "Alcohol abuse",
    "Depression",
    "AAFA001",
    "AAFA002",
    "AAFA007",
    "AALA004",
    "AALB001",
    "AALB002",
    "ABCB001",
    "ABLB003",
    "ACFA002",
    "ACFA007",
    "ACFA013",
    "ACFA028",
    "ACPA001",
    "AFLB008",
    "AGLB001",
    "DBKA006",
    "DBMA002",
    "DDAF008",
    "DDMA031",
    "DDQH009",
    "DENF018",
    "DEPF033",
    "DEQF004",
    "DZEA002",
    "EASF010",
    "EBFA008",
    "EBLA003",
    "EPLF005",
    "EQLF004",
    "EQQF004",
    "GEJE001",
    "GEQE004",
    "GEQE007",
    "GEQE009",
    "GEQE012",
    "HEQE002",
    "HHFA016",
    "HJQE001",
    "HPFA003",
    "HPFA004",
    "JAEA003",
    "JQGA002",
    "JQGA003",
    "JQGA004",
    "JVJF002",
    "JVJF004",
    "JVJF005",
    "JVJF008",
    "KCFA005",
    "NBCA006",
    "NBCA010",
    "NEKA020",
    "ZCQA001",
    "dureeurg",
    "ccmu",
    "igs2",
    "cmuc",
    "went_to_emergency_care",
    "RUMNU",
]

emb_cols = ordinal_cols + one_hot_cols

# Numerical columns
cont_cols = (
    [
        "age",
        "mean_los_um_n_days",
        "mean_los_diag_n_days",
        "mean_los_n_days",
        "n_adm_n_days",
        "n_adm_n_days_diag",
        "n_adm_n_days_um",
        "n_adm_n_days_patient",
        "time_already_spent_stay_patient",
        # "time_already_spent_n_days_patient",
        "Number of elixhauser commorbidities",
        "Number of commorbidities",
        "n_acts",
        "urg_n_commorbidities",
        "urg_n_acts",
        "urg_adm_n_days",
        "urg_n_adm_n_days_diag",
        "urg_n_adm_n_days_patient",
    ]
    + ["mean_los_diag_cat_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
    # + ["mean_los_diag_cat_um_n_days_q" + str(q) for q in np.arange(0.25, 1.25, 0.25)]
)

x_df, y_df, ordinal_enc, label_enc, cont_enc = dataset.format_dataset_dl(
    "../notebooks/pmsi_full.csv",
    ordinal_cols,
    cont_cols,
)

print("training on {} RUM".format(len(x_df)))
print("training on {} patients".format(len(x_df["ippano"].unique())))
print("training on {} stays".format(len(x_df["rssnuano"].unique())))

# Remove los_cont since it's no longer needed
x_df.drop(
    [
        # "dentdow",
        "level_0",
        "Unnamed: 0",
        "los_cont",
        "rssnuano",
        "ippano",
    ],
    axis=1,
    inplace=True,
)

# this makes it easier to select categorical and numerical columns
x_df = x_df.reindex(columns=ordinal_cols + cont_cols, copy=False)

# save for eval afterward
x_df.to_csv("x_df_to_eval.csv")
y_df.to_csv("y_df_to_eval.csv")
with open("ord_enc_to_eval", "wb") as fh:
    pickle.dump(ordinal_enc, fh)
with open("label_enc_to_eval", "wb") as fh:
    pickle.dump(label_enc, fh)
with open("cont_enc_to_eval", "wb") as fh:
    pickle.dump(cont_enc, fh)


train_dl_binary(x_df, y_df, ordinal_enc, ordinal_cols, cont_cols, label_enc)
end = datetime.now()
print("Took {}".format(end - start))
