# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine
# Duclos, Julien Fondrevelle, RESHAPE Inserm Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import torch
from collections.abc import Iterable


class los(torch.nn.Module):
    def __init__(
        self,
        emb_szs: list,
        n_cont: int,
        out_sz,
        layers: list = [256, 128, 64, 32],
        y_range=None,
        ps=None,
        use_bn=False,
        bn_final=False,
        emb_drop=0.05,
        emb_sz_factor=7,
    ):
        super(los, self).__init__()
        # Taking inspiration from fast AI implementation
        ps = self.ifnone(ps, [0] * len(layers))
        ps = self.listify(ps, layers)
        self.emb_drop = torch.nn.Dropout(emb_drop)
        self.emb_sz_factor = emb_sz_factor
        self.softmax = torch.nn.LogSoftmax(dim=1)
        emb_szs = [(n_cat, self.emb_sz_rule(n_cat)) for n_cat in emb_szs]
        self.embeds = torch.nn.ModuleList(
            [self.embedding(ni, nf) for ni, nf in emb_szs]
        )
        n_emb = sum(e.embedding_dim for e in self.embeds)
        self.n_emb, self.n_cont, self.y_range = n_emb, n_cont, y_range
        sizes = self.get_sizes(layers, out_sz)
        actns = [torch.nn.SELU(inplace=True) for _ in range(len(sizes) - 2)] + [None]
        layers = []
        for i, (n_in, n_out, dp, act) in enumerate(
            zip(sizes[:-1], sizes[1:], [0.0] + ps, actns)
        ):
            layers += self.bn_drop_lin(
                n_in, n_out, bn=use_bn and i != 0, p=dp, actn=act
            )
        if bn_final:
            layers.append(torch.nn.BatchNorm1d(sizes[-1]))

        self.layers = torch.nn.Sequential(*layers)

    def bn_drop_lin(
        self, n_in: int, n_out: int, bn: bool = True, p: float = 0.0, actn=None
    ):
        """Sequence of batchnorm (if `bn`), dropout (with `p`) and linear
        (`n_in`,`n_out`) layers followed by `actn`."""

        layers = [torch.nn.BatchNorm1d(n_in)] if bn else []
        if p != 0:
            # AlphaDropout preserme normalization of the layers (keep mean and
            # variance stays the same
            layers.append(torch.nn.AlphaDropout(p))
        layers.append(torch.nn.Linear(n_in, n_out))
        if actn is not None:
            layers.append(actn)

        return layers

    def get_sizes(self, layers, out_sz):

        return [self.n_emb + self.n_cont] + layers + [out_sz]

    def forward(
        self, x_cat: torch.Tensor, x_cont: torch.Tensor, captum: bool = False
    ) -> torch.Tensor:
        if self.n_emb != 0 and not captum:
            x = [e(x_cat[:, i]) for i, e in enumerate(self.embeds, start=0)]
            x = torch.cat(x, 1)
            x = self.emb_drop(x)
            x_cat = x
        x = torch.cat([x_cat, x_cont], 1)
        x = self.layers(x)
        if self.y_range is not None:
            x = (self.y_range[1] - self.y_range[0]) * torch.sigmoid(x) + self.y_range[0]

        return self.softmax(x)

    def emb_sz_rule(self, n_cat: int) -> int:
        return min(600, round(self.emb_sz_factor * n_cat**0.56))

    def embedding(self, ni: int, nf: int) -> torch.nn.Module:
        "Create an embedding layer."
        emb = torch.nn.Embedding(ni, nf)
        # See https://arxiv.org/abs/1711.09160
        with torch.no_grad():
            self.trunc_normal_(emb.weight, std=0.01)

        return emb

    def trunc_normal_(
        self, x: torch.Tensor, mean: float = 0.0, std: float = 1.0
    ) -> torch.Tensor:
        "Truncated normal initialization."
        # From https://discuss.pytorch.org/t/implementing-truncated-normal-initializer/4778/12
        return x.normal_().fmod_(2).mul_(std).add_(mean)

    @staticmethod
    def ifnone(a, b):
        "`a` if `a` is not None, otherwise `b`."

        return b if a is None else a

    @staticmethod
    def listify(p=None, q=None):
        "Make `p` listy and the same length as `q`."
        if p is None:
            p = []
        elif isinstance(p, str):
            p = [p]
        elif not isinstance(p, Iterable):
            p = [p]
        # Rank 0 tensors in PyTorch are Iterable but don't have a length.
        else:
            try:
                a = len(p)
            except:
                p = [p]
        n = q if type(q) == int else len(p) if q is None else len(q)
        if len(p) == 1:
            p = p * n

        assert len(p) == n, f"List len mismatch ({len(p)} vs {n})"

        return list(p)


class los_n_emb(torch.nn.Module):
    """neural network without embeddings"""

    def __init__(
        self,
        emb_szs: list,
        n_cont: int,
        out_sz,
        layers: list = [256, 128, 64, 32],
        y_range=None,
        ps=None,
        use_bn=False,
        bn_final=False,
    ):
        super(los_n_emb, self).__init__()
        ps = self.ifnone(ps, [0] * len(layers))
        ps = self.listify(ps, layers)
        self.n_emb = emb_szs
        self.bn_cont = torch.nn.BatchNorm1d(n_cont)
        self.n_cont, self.y_range = n_cont, y_range
        self.softmax = torch.nn.LogSoftmax(dim=1)
        sizes = self.get_sizes(layers, out_sz)
        actns = [torch.nn.SELU(inplace=True) for _ in range(len(sizes) - 2)] + [None]
        layers = []
        for i, (n_in, n_out, dp, act) in enumerate(
            zip(sizes[:-1], sizes[1:], [0.0] + ps, actns)
        ):
            layers += self.bn_drop_lin(
                n_in, n_out, bn=use_bn and i != 0, p=dp, actn=act
            )
        if bn_final:
            layers.append(torch.nn.BatchNorm1d(sizes[-1]))

        self.layers = torch.nn.Sequential(*layers)

    def bn_drop_lin(
        self, n_in: int, n_out: int, bn: bool = True, p: float = 0.0, actn=None
    ):
        """Sequence of batchnorm (if `bn`), dropout (with `p`) and linear
        (`n_in`,`n_out`) layers followed by `actn`."""

        layers = [torch.nn.BatchNorm1d(n_in)] if bn else []
        if p != 0:
            # AlphaDropout preserme normalization of the layers (keep mean and
            # variance stays the same
            layers.append(torch.nn.AlphaDropout(p))
        layers.append(torch.nn.Linear(n_in, n_out))
        if actn is not None:
            layers.append(actn)

        return layers

    def get_sizes(self, layers, out_sz):

        return [self.n_emb + self.n_cont] + layers + [out_sz]

    def forward(self, x_cat: torch.Tensor, x_cont: torch.Tensor) -> torch.Tensor:
        x = torch.cat([x_cat, x_cont], 1)
        x = self.layers(x)
        if self.y_range is not None:
            x = (self.y_range[1] - self.y_range[0]) * torch.sigmoid(x) + self.y_range[0]

        return self.softmax(x)

    @staticmethod
    def ifnone(a, b):
        "`a` if `a` is not None, otherwise `b`."

        return b if a is None else a

    @staticmethod
    def listify(p=None, q=None):
        "Make `p` listy and the same length as `q`."
        if p is None:
            p = []
        elif isinstance(p, str):
            p = [p]
        elif not isinstance(p, Iterable):
            p = [p]
        # Rank 0 tensors in PyTorch are Iterable but don't have a length.
        else:
            try:
                a = len(p)
            except:
                p = [p]
        n = q if type(q) == int else len(p) if q is None else len(q)
        if len(p) == 1:
            p = p * n

        assert len(p) == n, f"List len mismatch ({len(p)} vs {n})"

        return list(p)


class los_logit(torch.nn.Module):
    """Return logits directly instead of using LogSoftmax"""

    def __init__(
        self,
        emb_szs: list,
        n_cont: int,
        out_sz,
        layers: list = [256, 128, 64, 32],
        y_range=None,
        ps=None,
        use_bn=False,
        bn_final=False,
        emb_drop=0.05,
        emb_sz_factor=7,
    ):
        super(los_logit, self).__init__()
        ps = self.ifnone(ps, [0] * len(layers))
        ps = self.listify(ps, layers)
        self.emb_drop = torch.nn.Dropout(emb_drop)
        self.emb_sz_factor = emb_sz_factor
        emb_szs = [(n_cat, self.emb_sz_rule(n_cat)) for n_cat in emb_szs]
        self.embeds = torch.nn.ModuleList(
            [self.embedding(ni, nf) for ni, nf in emb_szs]
        )
        n_emb = sum(e.embedding_dim for e in self.embeds)
        self.n_emb, self.n_cont, self.y_range = n_emb, n_cont, y_range
        print(self.n_cont)
        print(self.n_cont)
        sizes = self.get_sizes(layers, out_sz)
        actns = [torch.nn.SELU(inplace=True) for _ in range(len(sizes) - 2)] + [None]
        layers = []
        for i, (n_in, n_out, dp, act) in enumerate(
            zip(sizes[:-1], sizes[1:], [0.0] + ps, actns)
        ):
            layers += self.bn_drop_lin(
                n_in, n_out, bn=use_bn and i != 0, p=dp, actn=act
            )
        if bn_final:
            layers.append(torch.nn.BatchNorm1d(sizes[-1]))

        self.layers = torch.nn.Sequential(*layers)

    def bn_drop_lin(
        self, n_in: int, n_out: int, bn: bool = True, p: float = 0.0, actn=None
    ):
        """Sequence of batchnorm (if `bn`), dropout (with `p`) and linear
        (`n_in`,`n_out`) layers followed by `actn`."""

        layers = [torch.nn.BatchNorm1d(n_in)] if bn else []
        if p != 0:
            # AlphaDropout preserme normalization of the layers (keep mean and
            # variance stays the same
            layers.append(torch.nn.AlphaDropout(p))
        layers.append(torch.nn.Linear(n_in, n_out))
        if actn is not None:
            layers.append(actn)

        return layers

    def get_sizes(self, layers, out_sz):

        return [self.n_emb + self.n_cont] + layers + [out_sz]

    def forward(self, x_cat: torch.Tensor, x_cont: torch.Tensor) -> torch.Tensor:
        if self.n_emb != 0:
            x = [e(x_cat[:, i]) for i, e in enumerate(self.embeds, start=0)]
            x = torch.cat(x, 1)
            x = self.emb_drop(x)
        x = torch.cat([x, x_cont], 1)
        x = self.layers(x)
        if self.y_range is not None:
            x = (self.y_range[1] - self.y_range[0]) * torch.sigmoid(x) + self.y_range[0]

        return x

    def emb_sz_rule(self, n_cat: int) -> int:
        return min(600, round(self.emb_sz_factor * n_cat**0.56))

    def embedding(self, ni: int, nf: int) -> torch.nn.Module:
        "Create an embedding layer."
        emb = torch.nn.Embedding(ni, nf)
        # See https://arxiv.org/abs/1711.09160
        with torch.no_grad():
            self.trunc_normal_(emb.weight, std=0.01)

        return emb

    def trunc_normal_(
        self, x: torch.Tensor, mean: float = 0.0, std: float = 1.0
    ) -> torch.Tensor:
        "Truncated normal initialization."
        # From https://discuss.pytorch.org/t/implementing-truncated-normal-initializer/4778/12
        return x.normal_().fmod_(2).mul_(std).add_(mean)

    @staticmethod
    def ifnone(a, b):
        "`a` if `a` is not None, otherwise `b`."

        return b if a is None else a

    @staticmethod
    def listify(p=None, q=None):
        "Make `p` listy and the same length as `q`."
        if p is None:
            p = []
        elif isinstance(p, str):
            p = [p]
        elif not isinstance(p, Iterable):
            p = [p]
        # Rank 0 tensors in PyTorch are Iterable but don't have a length.
        else:
            try:
                a = len(p)
            except:
                p = [p]
        n = q if type(q) == int else len(p) if q is None else len(q)
        if len(p) == 1:
            p = p * n

        assert len(p) == n, f"List len mismatch ({len(p)} vs {n})"

        return list(p)
