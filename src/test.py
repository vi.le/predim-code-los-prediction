# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import pandas as pd
import pickle
from model import los, los_n_emb
import torch
from evaluate import evaluate, get_preds
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, cohen_kappa_score
from sklearn.dummy import DummyClassifier
import matplotlib.pyplot as plt
import numpy as np
from torchinfo import summary

plt.rcParams.update(
    {
        "text.usetex": True,
        "font.family": "sans-serif",
    }
)


x_df = pd.read_csv("x_df_to_eval.csv")
y_df = pd.read_csv("y_df_to_eval.csv")
with open("ord_enc_to_eval", "rb") as fh:
    ord_enc = pickle.load(fh)
with open("label_enc_to_eval", "rb") as fh:
    label_enc = pickle.load(fh)
with open("cont_enc_to_eval", "rb") as fh:
    cont_enc = pickle.load(fh)


x_df.drop(
    [
        "Unnamed: 0",
    ],
    axis=1,
    inplace=True,
)

y_df.drop(
    [
        "Unnamed: 0",
    ],
    axis=1,
    inplace=True,
)

cont_cols = [
    "age",
    "mean_los_um_n_days",
    "mean_los_diag_n_days",
    "mean_los_n_days",
    "n_adm_n_days",
    "n_adm_n_days_diag",
    "n_adm_n_days_um",
    "n_adm_n_days_patient",
    "time_already_spent_stay_patient",
    # "time_already_spent_n_days_patient",
    "Number of elixhauser commorbidities",
    "Number of commorbidities",
    "n_acts",
    "urg_n_commorbidities",
    "urg_n_acts",
    "urg_adm_n_days",
    "urg_n_adm_n_days_diag",
    "urg_n_adm_n_days_patient",
] + ["mean_los_diag_cat_n_days_q" + str(q) for q in range(1, 5)]

ordinal_cols = [
    "diag_category",
    "urg_diag_category",
    "dentmo",
    "denta",
    "dentdow",
    "denttod",
    "hopital",
    "typeauto",
    "umano",
    "sexe",
    "modent",
    "Congestive heart failure",
    "Cardiac arrhythmias",
    "Valvular disease",
    "Pulmonary circulation disorders",
    "Peripheral vascular disorders",
    "Hypertension uncomplicated",
    "Hypertension complicated",
    "Paralysis",
    "Other neurological disorders",
    "Chronic pulmonary disease",
    "Diabetes uncomplicated",
    "Diabetes complicated",
    "Hypothyroidism",
    "Renal failure",
    "Liver disease",
    "Metastatic cancer",
    "Solid tumour without metastasis",
    "Rheumatoid arthritis/collagen vascular diseases",
    "Coagulopathy",
    "Obesity",
    "Weight loss",
    "Fluid and electrolyte disorders",
    "Blood loss anaemia",
    "Deficiency anaemia",
    "Alcohol abuse",
    "Depression",
    "AAFA001",
    "AAFA002",
    "AAFA007",
    "AALA004",
    "AALB001",
    "AALB002",
    "ABCB001",
    "ABLB003",
    "ACFA002",
    "ACFA007",
    "ACFA013",
    "ACFA028",
    "ACPA001",
    "AFLB008",
    "AGLB001",
    "DBKA006",
    "DBMA002",
    "DDAF008",
    "DDMA031",
    "DDQH009",
    "DENF018",
    "DEPF033",
    "DEQF004",
    "DZEA002",
    "EASF010",
    "EBFA008",
    "EBLA003",
    "EPLF005",
    "EQLF004",
    "EQQF004",
    "GEJE001",
    "GEQE004",
    "GEQE007",
    "GEQE009",
    "GEQE012",
    "HEQE002",
    "HHFA016",
    "HJQE001",
    "HPFA003",
    "HPFA004",
    "JAEA003",
    "JQGA002",
    "JQGA003",
    "JQGA004",
    "JVJF002",
    "JVJF004",
    "JVJF005",
    "JVJF008",
    "KCFA005",
    "NBCA006",
    "NBCA010",
    "NEKA020",
    "ZCQA001",
    "dureeurg",
    "ccmu",
    "igs2",
    "cmuc",
    "went_to_emergency_care",
    "RUMNU",
]

model = los(
    layers=[512, 256, 192, 128, 64, 32],
    emb_sz_factor=7.3,
    use_bn=False,
    ps=0,
    emb_drop=0,
    # emb_szs=len(ordinal_cols),
    emb_szs=[len(cats) for cats in ord_enc.categories_],
    n_cont=len(cont_cols),
    out_sz=len(label_enc.classes_),
)


for fold in range(1, 6):

    checkpoint = torch.load("model_fold_" + str(fold))

    model.load_state_dict(checkpoint["model"])
    x_tst_fold = x_df.iloc[checkpoint["test_idx"]]
    y_tst_fold = y_df.iloc[checkpoint["test_idx"]]
    tst = torch.utils.data.TensorDataset(
        torch.Tensor(x_tst_fold.values), torch.LongTensor(y_tst_fold.values.squeeze())
    )
    tst_loader = torch.utils.data.DataLoader(tst, batch_size=1024, shuffle=False)

    dummy = DummyClassifier(strategy="stratified")
    dummy.fit(x_df.values, y_df.values)
    print("score dummy", dummy.score(x_tst_fold.values, y_tst_fold.values.squeeze()))
    dummy_preds = dummy.predict(x_tst_fold)
    print("kappa dummy", cohen_kappa_score(y_tst_fold.values, dummy_preds))

    # test_acc = evaluate(model, tst_loader, ordinal_cols)
    # print(test_acc)
    y_true, y_preds = get_preds(model, tst_loader, ordinal_cols, label_enc)
    print("Real kappa", cohen_kappa_score(y_true, y_preds, weights="linear"))

checkpoint = torch.load("model_fold_1")
print(model)

model.load_state_dict(checkpoint["model"])
x_tst_fold = x_df.iloc[checkpoint["test_idx"]]
y_tst_fold = y_df.iloc[checkpoint["test_idx"]]
tst = torch.utils.data.TensorDataset(
    torch.Tensor(x_tst_fold.values),
    torch.LongTensor(y_tst_fold.values.squeeze()),
)
tst_loader = torch.utils.data.DataLoader(tst, batch_size=1024, shuffle=False)
fig, ax = plt.subplots(1, 1, figsize=(16, 16))
y_true, y_preds = get_preds(model, tst_loader, ordinal_cols, label_enc)
classes = list(range(0, 14)) + ["14+"]
cm = confusion_matrix(y_true, y_preds, labels=classes)  # , normalize="true")
cm = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]
disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=classes)
disp.plot(ax=ax, cmap="Blues")  # , values_format=".1%")
classes = (
    ["0 days", "1 day"] + [str(day) + " days" for day in range(2, 14)] + ["14+ days"]
)
classes = [
    day
    + " ("
    + str(
        "{:,}".format(
            y_tst_fold.value_counts()[int(day.split(" ")[0].replace("+", ""))].values[0]
        )
    ).replace(",", " ")
    + " RUM)"
    for day in classes
]
ax.set_yticklabels(classes)
ax.set_title("Confusion matrix with test set size: " + str(len(y_true)))
disp.figure_.savefig("cm.eps", bbox_inches="tight", dpi=300)
disp.figure_.savefig("cm.png", bbox_inches="tight", dpi=300)
