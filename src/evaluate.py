# SPDX-FileCopyrightText: 2022 Vincent Lequertier <vincent@vl8r.eu>, Antoine Duclos, Julien Fondrevelle, RESHAPE Inserm U1290, DISP UR4570
#
# SPDX-License-Identifier: GPL-3.0-only

import torch
from sklearn.metrics import cohen_kappa_score
import numpy as np


def evaluate(model, loader, ordinal_cols):
    model.eval()
    total = 0
    correct = 0
    loss = []
    kappa = []
    with torch.inference_mode():
        for (x, y) in loader:
            x_cat = x[:, : len(ordinal_cols)].long()
            x_cont = x[:, len(ordinal_cols) :]
            preds = model(x_cat, x_cont)
            loss.append(torch.nn.functional.nll_loss(preds, y))
            preds = preds.argmax(dim=1)
            total += x.size(0)
            correct += (preds == y).sum().item()
            kappa.append(cohen_kappa_score(preds.numpy(), y.numpy(), weights="linear"))

    model.train()
    return (
        (correct / total),
        (sum(loss) / len(loss)).item(),
        (sum(kappa) / len(kappa)),
    )


def evaluate_reg(model, loader, ordinal_cols):
    model.eval()
    with torch.no_grad():
        loss = []
        total = []
        for (x, y) in loader:
            x_cat = x[:, : len(ordinal_cols)].long()
            x_cont = x[:, len(ordinal_cols) :]
            preds = model(x_cat, x_cont)
            total.append(torch.sqrt(torch.nn.functional.mse_loss(preds, y)))
            loss.append(torch.nn.functional.mse_loss(preds, y))
    model.train()

    return (sum(total) / len(total)).item(), (sum(loss) / len(loss)).item()


def get_preds(model, loader, ordinal_cols, label_enc):
    model.eval()
    y_true = []
    y_preds = []
    with torch.inference_mode():
        for (x, y) in loader:
            x_cat = x[:, : len(ordinal_cols)].long()
            x_cont = x[:, len(ordinal_cols) :]
            preds = model(x_cat, x_cont).argmax(dim=1)
            y = label_enc.inverse_transform(y)
            preds = label_enc.inverse_transform(preds)
            y_true.append(y)
            y_preds.append(preds)

    model.train()
    # Unroll (remove the batch dimension)
    return np.concatenate(y_true, axis=0), np.concatenate(y_preds, axis=0)


def get_preds_binary(model, loader, ordinal_cols, label_enc):
    model.eval()
    y_true = []
    y_preds = []
    with torch.inference_mode():
        for (x, y) in loader:
            x_cat = x[:, : len(ordinal_cols)].long()
            x_cont = x[:, len(ordinal_cols) :]
            preds = model(x_cat, x_cont)
            probs = torch.nn.functional.softmax(preds, dim=1)[:, 1]
            preds = torch.nn.functional.log_softmax(preds, dim=1).argmax(dim=1)
            y = label_enc.inverse_transform(y)
            preds = label_enc.inverse_transform(preds)
            y_true.append(y)
            y_preds.append(preds)

    model.train()
    # Unroll (remove the batch dimension)
    return np.concatenate(y_true, axis=0), np.concatenate(y_preds, axis=0)


def get_probs_binary(model, loader, ordinal_cols, label_enc):
    model.eval()
    y_true = []
    y_preds = []
    with torch.inference_mode():
        for (x, y) in loader:
            x_cat = x[:, : len(ordinal_cols)].long()
            x_cont = x[:, len(ordinal_cols) :]
            preds = model(x_cat, x_cont)
            probs = torch.nn.functional.softmax(preds, dim=1)[:, 1]
            y_true.append(y)
            y_preds.append(probs)

    model.train()
    # Unroll (remove the batch dimension)
    return np.concatenate(y_true, axis=0), np.concatenate(y_preds, axis=0)


def get_probs(model, loader, ordinal_cols, label_enc):
    model.eval()
    y_true = []
    y_preds = []
    with torch.inference_mode():
        for (x, y) in loader:
            x_cat = x[:, : len(ordinal_cols)].long()
            x_cont = x[:, len(ordinal_cols) :]
            preds = model(x_cat, x_cont)[:, 1]
            probs = torch.pow(10, preds)
            y_preds.append(probs)
            y_true.append(y)

    model.train()
    # Unroll (remove the batch dimension)
    print(torch.stack(y_preds, dim=1).shape)
    return ([y for x in y_true for y in x],)
